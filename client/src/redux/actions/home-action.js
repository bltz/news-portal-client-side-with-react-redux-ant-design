import axios from 'axios';
import { showLoading, hideLoading } from 'react-redux-loading-bar';
const hostAddress = 'http://localhost:3000/'
export const fetchHeadlineNewsFulfilled = (dataResponse) => {
  return {
    type : 'FETCH_HEADLINE_FULFILLED',
    payload : {
      headlineNews : dataResponse
    }
  }
}

export const fetchHeadlineNewsRejected = (errResponse) => {
  return {
    type : 'FETCH_HEADLINE_REJECTED',
    payload : {
      headlineNews : errResponse
    }
  }
}

export const fetchLastesNewsFulfilled = (dataResponse) => {
  return {
    type : 'FETCH_LASTESNEWS_FULFILLED',
    payload : {
      lastesNews : dataResponse
    }
  }
}

export const fetchLastNewsRejected = (errResponse) => {
  return {
    type : 'FETCH_LASTESNEWS_REJECTED',
    payload : {
      lastesNews : errResponse
    }
  }
}

export const fetchSponsoredNewsFulfilled = (dataResponse) => {
  return {
    type : 'FETCH_SPONSOREDNEWS_FULFILLED',
    payload : {
      sponsoredNews : dataResponse
    }
  }
}

export const fetchSponsoredNewsRejected = (errResponse) => {
  return {
    type : 'FETCH_SPONSOREDNEWS_REJECTED',
    payload : {
      sponsoredNews : errResponse
    }
  }
}

export const fetchPopularNewsFulfilled = (dataResponse) => {
  return {
    type : 'FETCH_POPULAR_FULFILLED',
    payload : {
      popularNews : dataResponse
    }
  }
}

export const fetchPopularNewsRejected = (errResponse) => {
  return {
    type : 'FETCH_POPULAR_REJECTED',
    payload : {
      popularNews : errResponse
    }
  }
}

export const fetchNasionalNewsFulfilled = (dataResponse) => {
  return {
    type : 'FETCH_NASIONAL_FULFILLED',
    payload : {
      nasionalNews : dataResponse
    }
  }
}

export const fetchNasionalNewsRejected = (errResponse) => {
  return {
    type : 'FETCH_NASIONAL_REJECTED',
    payload : {
      nasionalNews : errResponse
    }
  }
}

export const fetchInternasionalNewsFulfilled = (dataResponse) => {
  return {
    type : 'FETCH_INTERNASIONAL_FULFILLED',
    payload : {
      internasionalNews : dataResponse
    }
  }
}

export const fetchInternasionalNewsRejected = (errResponse) => {
  return {
    type : 'FETCH_INTERNASIONAL_REJECTED',
    payload : {
      internasionalNews : errResponse
    }
  }
}

export const fetchRayaDaerahNewsFulfilled = (dataResponse) => {
  return {
    type : 'FETCH_RAYADAERAH_FULFILLED',
    payload : {
      rayaDaerahNews : dataResponse
    }
  }
}

export const fetchRayaDaerahNewsRejected = (errResponse) => {
  return {
    type : 'FETCH_RAYADAERAH_REJECTED',
    payload : {
      rayaDaerahNews : errResponse
    }
  }
}

export const fetchRayaPopNewsFulfilled = (dataResponse) => {
  return {
    type : 'FETCH_RAYAPOP_FULFILLED',
    payload : {
      rayaPopNews : dataResponse
    }
  }
}

export const fetchRayaPopNewsRejected = (errResponse) => {
  return {
    type : 'FETCH_RAYAPOP_REJECTED',
    payload : {
      rayaPopNews : errResponse
    }
  }
}

export const fetchRayaSportNewsFulfilled = (dataResponse) => {
  return {
    type : 'FETCH_RAYASPORT_FULFILLED',
    payload : {
      rayaSportNews : dataResponse
    }
  }
}

export const fetchRayaSportNewsRejected = (errResponse) => {
  return {
    type : 'FETCH_RAYASPORT_REJECTED',
    payload : {
      rayaSportNews : errResponse
    }
  }
}

export const fetchRayaOtoNewsFulfilled = (dataResponse) => {
  return {
    type : 'FETCH_RAYAOTO_FULFILLED',
    payload : {
      rayaOtoNews : dataResponse
    }
  }
}

export const fetchRayaOtoNewsRejected = (errResponse) => {
  return {
    type : 'FETCH_RAYAOTO_REJECTED',
    payload : {
      rayaOtoNews : errResponse
    }
  }
}

export const fetchRayaTripNewsFulfilled = (dataResponse) => {
  return {
    type : 'FETCH_RAYATRIP_FULFILLED',
    payload : {
      rayaTripNews : dataResponse
    }
  }
}

export const fetchRayaTripNewsRejected = (errResponse) => {
  return {
    type : 'FETCH_RAYATRIP_REJECTED',
    payload : {
      rayaTripNews : errResponse
    }
  }
}

export const fetchRayaChannelNewsFulfilled = (dataResponse) => {
  return {
    type : 'FETCH_RAYACHANNEL_FULFILLED',
    payload : {
      rayaChannelNews : dataResponse
    }
  }
}

export const fetchRayaChannelNewsRejected = (errResponse) => {
  return {
    type : 'FETCH_RAYACHANNEL_REJECTED',
    payload : {
      rayaChannelNews : errResponse
    }
  }
}

export function fetchHomeReq(){
  return dispatch => {
    dispatch(showLoading()) 
    axios.get(`${hostAddress}index/home/headline?l=6&vlugcvlhbOokn`).then((response) => {
      response.data.response ? dispatch(fetchHeadlineNewsRejected(response.data.response)) : dispatch(fetchHeadlineNewsFulfilled(response.data))
      axios.get(`${hostAddress}query/news/latest?q=news+index&l=6`).then((response) => {
        response.data.response ? dispatch(fetchLastNewsRejected(response.data.response)) : dispatch(fetchLastesNewsFulfilled(response.data))
        axios.get(`${hostAddress}query/news/isSponsored?q=news+index+&iXD6M&XgX7Wonye1$@U4Z1g3D&l=1`).then((response) => {
          response.data.response ? dispatch(fetchSponsoredNewsRejected(response.data.response)) : dispatch(fetchSponsoredNewsFulfilled(response.data))
            axios.get(`${hostAddress}news/popular?l=5&q=3-days-ago`).then((response) => {
              response.data.response ? dispatch(fetchPopularNewsRejected(response.data.response)) : dispatch(fetchPopularNewsFulfilled(response.data))
              axios.get(`${hostAddress}categorys/nasional?l=4`).then((response) => {
                response.data.response ? dispatch(fetchNasionalNewsRejected(response.data.response)) : dispatch(fetchNasionalNewsFulfilled(response.data))
                axios.get(`${hostAddress}categorys/internasional?l=4`).then((response) => {
                  response.data.response ? dispatch(fetchInternasionalNewsRejected(response.data.response)) : dispatch(fetchInternasionalNewsFulfilled(response.data))
                  axios.get(`${hostAddress}categorys/raya-daerah?l=4`).then((response) => {
                    response.data.response ? dispatch(fetchRayaDaerahNewsRejected(response.data.response)) : dispatch(fetchRayaDaerahNewsFulfilled(response.data))
                    axios.get(`${hostAddress}categorys/raya-pop?l=4`).then((response) => {
                      response.data.response ? dispatch(fetchRayaPopNewsRejected(response.data.response)) : dispatch(fetchRayaPopNewsFulfilled(response.data))
                      axios.get(`${hostAddress}categorys/raya-sport?l=1`).then((response) => {
                        response.data.response ? dispatch(fetchRayaSportNewsRejected(response.data.response)) : dispatch(fetchRayaSportNewsFulfilled(response.data))
                        axios.get(`${hostAddress}categorys/raya-oto?l=1`).then((response) => {
                          response.data.response ? dispatch(fetchRayaOtoNewsRejected(response.data.response)) : dispatch(fetchRayaOtoNewsFulfilled(response.data))
                          axios.get(`${hostAddress}categorys/raya-trip?l=1`).then((response) => {
                            response.data.response ? dispatch(fetchRayaTripNewsRejected(response.data.response)) : dispatch(fetchRayaTripNewsFulfilled(response.data))
                            axios.get(`${hostAddress}categorys/raya-channel?l=1`).then((response) => {
                              response.data.response ? dispatch(fetchRayaChannelNewsRejected(response.data.response)) : dispatch(fetchRayaChannelNewsFulfilled(response.data))
                              dispatch(hideLoading())
                            }).catch((err) => {
                              dispatch(fetchRayaChannelNewsRejected(err))
                            })
                          }).catch((err) => {
                            dispatch(fetchRayaTripNewsRejected(err))
                          })
                        }).catch((err) => {
                          dispatch(fetchRayaOtoNewsRejected(err))
                        })
                      }).catch((err) => {
                        dispatch(fetchRayaSportNewsRejected(err))
                      })
                    }).catch((err) => {
                      dispatch(fetchRayaPopNewsRejected(err))
                    })
                  }).catch((err) => {
                    dispatch(fetchRayaDaerahNewsRejected(err))
                  })
                }).catch((err) => {
                  dispatch(fetchInternasionalNewsRejected(err))
                })
              }).catch((err) => {
                dispatch(fetchNasionalNewsRejected(err))
              })
            }).catch((err) => {
              dispatch(fetchPopularNewsRejected(err))
            })
          }).catch((err) => {
            dispatch(fetchSponsoredNewsRejected(err))
          })
      }).catch((err) => {
        dispatch(fetchLastNewsRejected(err))
      })
    }).catch((err) => {
      dispatch(fetchHeadlineNewsRejected(err))
      setTimeout(() => {
        dispatch(hideLoading())
      }, 1000);
    })
    
  }
}