import axios from 'axios';
import { showLoading, hideLoading } from 'react-redux-loading-bar';
const hostAddress = 'http://localhost:3000/'

export const updateUsername = (data) => {
  return {
    type : 'UPDATE_USERNAME',
    payload : {
      data
    }
  }
}

export const updatePassword = (data) => {
  return {
    type : 'UPDATE_PASSWORD',
    payload : {
      data
    }
  }
}
export const responseMsg = (msg, status) => {
  return {
    type : 'USER_LOGIN_RESPONSE_MSG',
    payload : {
      msg,
      status
    }
  }
}
export const resetStatusLogin = () => {
  console.log('reset')
  return {
    type : 'RESET_STATUS_LOGIN',
    payload : {
      msg : null,
      status : null
    }
  }
}
export const loginSuccesFully = (msg,status,token,fullname,avatar) => {
  return {
    type : 'USER_LOGIN_SUCCESS',
    payload : {
      succes : true,
      msg,
      status,
      token,
      fullname,
      avatar,
      loginWith : 'local'
    }
  }
}

export const loginRequestWithSosmedSuccesFully = (msg,status,token,fullname,avatar,succes,provider) => {
  return {
    type : 'USER_LOGIN_WITH_SOSMED_SUCCESS',
    payload : {
      msg,
      status,
      token,
      fullname,
      avatar,
      loginWith : provider,
      succes
    }
  }
}

export function loginRequestWithSosmed(payload){
  return dispatch => {
    axios.post(`http://localhost:3000/users/auth/v3?provider=${payload.provider}`, payload).then((response) => {
      console.log(response,'><><><')
      if(response.data.success === true && response.data.status === 200){
        dispatch(loginRequestWithSosmedSuccesFully(
          response.data.response, 
          response.data.status, 
          response.data.token, 
          response.data.fullname, 
          response.data.avatar,
          response.data.success,
          payload.provider
        ))
        dispatch(hideLoading())
      }else{
        dispatch(responseMsg(response.data.response, response.data.status))
        dispatch(hideLoading())
      }
    }).catch((error) => {
      console.log(error.message,'????')
      // dispatch(responseMsg(response.data.response, response.data.status))
      // dispatch(hideLoading())
      if(error.message === `Network Error`){
        console.log('server is offline')
      }
    })
  }
}

export function loginRequest(username, password){
  return dispatch => {
    dispatch(showLoading())
    if(username !== 'username' && password !== 'password'){
      let payload={
        "username":username,
        "password":password
        }
      axios.post(`${hostAddress}users/login`, payload).then((response) => {
        console.log(response.data,'<<<<<')
        if(response.data.success === true && response.data.status === 200){
          dispatch(loginSuccesFully(
            response.data.response, 
            response.data.status, 
            response.data.token, 
            response.data.fullname, 
            response.data.avatar
          ))
          dispatch(hideLoading())
        }else{
          dispatch(responseMsg(response.data.response, response.data.status))
          dispatch(hideLoading())
        }
      }).catch((error) => {
        console.log(error.message,'????')
        // dispatch(responseMsg(response.data.response, response.data.status))
        // dispatch(hideLoading())
        if(error.message === `Network Error`){
          console.log('server is offline')
        }
      })
    }else{
      console.log('request login masih sama aja', username , password)
      dispatch(responseMsg('request login masih sama aja', 202))
      dispatch(hideLoading())
    }
  }
}