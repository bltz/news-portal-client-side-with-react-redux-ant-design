import axios from 'axios';
import { showLoading, hideLoading } from 'react-redux-loading-bar';
const hostAddress = 'http://localhost:3000/' 

export const rejectSingleArticleRequest = (data, slug) => {
  return {
    type : 'REJECT_REQ_SINGLE_ARTICLE',
    payload : {
      data,
      slug
    }
  }
}

export const fetchSingleArticleRequest = (data) => {
  return {
    type : 'REQ_SINGLE_ARTICLE',
    payload : {
      data
    }
  }
}

export function singleArticleRequest(slug){
  return dispatch => {
    dispatch(showLoading())
    axios.get(`${hostAddress}news/${slug}`).then((response) => {
      if(response.data.response === "::: NO DATA SINGLE NEWS BY SLUG :::"){
        console.log(response.data,':XX:x:XX:')
        dispatch(rejectSingleArticleRequest(response.data, slug))
        dispatch(hideLoading())
      }else{
        console.log(response.data,':x:x:x:')
        dispatch(fetchSingleArticleRequest(response.data))
        dispatch(hideLoading())
      }
    }).catch((err) => {
      console.log(err,'>>>>>')
      dispatch(hideLoading())
    })
  }
}

