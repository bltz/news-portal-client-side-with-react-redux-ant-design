import axios from 'axios';
import { showLoading, hideLoading } from 'react-redux-loading-bar';
const hostAddress = 'http://localhost:3000/'

export const partialSearch = (data,value) => {
  return {
    type : 'PARTIAL_SEARCH',
    payload : {
      data,
      value
    }
  }
}

export const openResultPanel = (data, value) => {
  return {
    type : 'OPEN_PARTIAL_SEARCH_PANEL',
    payload : {
      data,
      value
    }
  }
}

export const closeResultPanel = (data, reset) => {
  return {
    type : 'VALUE_LENGTH_IS_0',
    payload : {
      data,
      reset
    }
  }
}

export const resetValuePartialSearch = (data) => {
  return {
    type : 'RESET_VALUE',
    payload : {
      data
    }
  }
}

export function cloasePanelSearch(){
  return dispatch => {
    dispatch(closeResultPanel('close',''))
  }
}

export function resetValueSearch(){
  return dispatch => {
    dispatch(resetValuePartialSearch(''))
  }
}

export function partialSearchRequest(value){
  return dispatch => {
    dispatch(showLoading())
    if(value.length !== 0){
      axios.get(`${hostAddress}query/search?q=${value}&l=8`).then((response)=>{
        // console.log(response.data,'::::::')
        dispatch(partialSearch(response.data, value))
        dispatch(openResultPanel('open',value))
        dispatch(hideLoading())
      }).catch((err)=>{
        console.log(err)
        dispatch(hideLoading())
      })
    }else{
      dispatch(closeResultPanel('close'))
      dispatch(hideLoading())
    }
  }
}