const initialState = {
  fetched : false,
  indeks : [],
  rejectMsg : null
}
const reducer = (state = initialState, {type,payload}) => {
  switch(type){
    case 'FETCH_RAYATRIP_FULFILLED':
      return {...state, indeks : payload.rayaTripNews, fetched : true}
    case 'FETCH_RAYATRIP_REJECTED':
      return {...state, rejectMsg : payload.rayaTripNews, fetched : false}
    default:
      return state;
  }
}

export default reducer;