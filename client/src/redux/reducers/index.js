import { combineReducers } from 'redux';
import { loadingBarReducer } from 'react-redux-loading-bar';

import headlineNews from './headlineNews-reducer'
import lastesNews from './lastesNews-reducer'
import sponsoredNews from './sponsoredNews-reducer'
import popularNews from './popularNews-reducer'
import nasionalNews from './nasionalNews-reducer'
import internasionalNews from './internasionalNews-reducer'
import rayaDaerahNews from './rayaDaerahNews-reducer'
import rayaPopNews from './rayaPopNews-reducer'
import rayaSportNews from './rayaSportNews-reducer'
import rayaOtoNews from './rayaOto-reducer'
import rayaTripNews from './rayaTrip-reducer'
import rayaChannelNews from './rayaChannel-reducer'
import userLogin from './userLogin-reducer'
import search from './search-reducer'
import singleArticle from './singleArticle-reducer'

const allReducers = combineReducers({
  headlineNews,
  lastesNews,
  sponsoredNews,
  popularNews,
  nasionalNews,
  internasionalNews,
  rayaDaerahNews,
  rayaPopNews,
  rayaSportNews,
  rayaOtoNews,
  rayaTripNews,
  rayaChannelNews,
  userLogin,
  loadingBar: loadingBarReducer,
  search,
  singleArticle
})

export default allReducers