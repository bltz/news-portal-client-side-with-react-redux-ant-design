const initialState = {
  username : 'username',
  password : 'password',
  responseMessage : null,
  responseStatus : null,
  succesLogin : false,
  token : null,
  fullname : null,
  avatar : null,
  loginWith : null
}

const reducer = (state = initialState, {type,payload}) => {
  switch(type){
    case 'UPDATE_USERNAME':
      return {
        ...state, 
        username : payload.data,
        responseMessage : null,
        responseStatus : null
      }
    case 'UPDATE_PASSWORD':
      return {
        ...state, 
        password : payload.data,
        responseMessage : null,
        responseStatus : null
      }
    case 'USER_LOGIN_RESPONSE_MSG':
      return {
        ...state, 
        responseMessage : payload.msg,
        responseStatus : payload.status
      }
    case 'RESET_STATUS_LOGIN':
      return {
        ...state, 
        username : 'username',
        password : 'password',
        responseMessage : payload.msg,
        responseStatus : payload.status,
        succesLogin : false,
        token : null,
        fullname : null,
        avatar : null,
        loginWith : null
      }
    case 'USER_LOGIN_SUCCESS':
      return {
        ...state, 
        username : 'u$ername',
        password : 'pa$$word',
        responseMessage : payload.msg,
        responseStatus : payload.status,
        succesLogin : payload.succes,
        token : payload.token,
        fullname : payload.fullname,
        avatar : payload.avatar,
        loginWith : payload.loginWith
      }
      case 'USER_LOGIN_WITH_SOSMED_SUCCESS':
      return {
        ...state, 
        username : 'u$ername',
        password : 'pa$$word',
        responseMessage : payload.msg,
        responseStatus : payload.status,
        succesLogin : payload.succes,
        token : payload.token,
        fullname : payload.fullname,
        avatar : payload.avatar,
        loginWith : payload.loginWith
      }
    default:
      return state;
  }
}

export default reducer;