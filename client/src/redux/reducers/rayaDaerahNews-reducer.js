const initialState = {
  fetched : false,
  indeks : [],
  rejectMsg : null
}

const reducer = (state = initialState, {type,payload}) => {
  switch(type){
    case 'FETCH_RAYADAERAH_FULFILLED':
      return {...state, indeks : payload.rayaDaerahNews, fetched : true}
    case 'FETCH_RAYADAERAH_REJECTED':
      return {...state, rejectMsg : payload.rayaDaerahNews, fetched : false}
    default:
      return state;
  }
}

export default reducer;