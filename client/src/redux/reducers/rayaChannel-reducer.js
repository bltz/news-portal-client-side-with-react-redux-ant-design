const initialState = {
  fetched : false,
  indeks : [],
  rejectMsg : null
}
const reducer = (state = initialState, {type,payload}) => {
  switch(type){
    case 'FETCH_RAYACHANNEL_FULFILLED':
      return {...state, indeks : payload.rayaChannelNews, fetched : true}
    case 'FETCH_RAYACHANNEL_REJECTED':
      return {...state, rejectMsg : payload.rayaChannelNews, fetched : false}
    default:
      return state;
  }
}

export default reducer;