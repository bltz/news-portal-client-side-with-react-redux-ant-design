const initialState = {
  fetched : false,
  indeks : [],
  rejectMsg : null
}

const reducer = (state = initialState, {type,payload}) => {
  switch(type){
    case 'FETCH_RAYAPOP_FULFILLED':
      return {...state, indeks : payload.rayaPopNews, fetched : true}
    case 'FETCH_RAYAPOP_REJECTED':
      return {...state, rejectMsg : payload.rayaPopNews, fetched : false}
    default:
      return state;
  }
}

export default reducer;