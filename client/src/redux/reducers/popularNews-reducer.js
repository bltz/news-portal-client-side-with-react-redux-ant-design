const initialState = {
  fetched : false,
  indeks : [],
  rejectMsg : null
}

const reducer = (state = initialState, {type,payload}) => {
  switch(type){
    case 'FETCH_POPULAR_FULFILLED':
      return {...state, indeks : payload.popularNews, fetched : true}
    case 'FETCH_POPULAR_REJECTED':
      return {...state, rejectMsg : payload.popularNews, fetched : false}
    default:
      return state;
  }
}

export default reducer;