const initialState = {
  fetched : false,
  indeks : [],
  rejectMsg : null
}
const reducer = (state = initialState, {type,payload}) => {
  switch(type){
    case 'FETCH_RAYAOTO_FULFILLED':
      return {...state, indeks : payload.rayaOtoNews, fetched : true}
    case 'FETCH_RAYAOTO_REJECTED':
      return {...state, rejectMsg : payload.rayaOtoNews, fetched : false}
    default:
      return state;
  }
}

export default reducer;