const initialState = {
  partial : [],
  valuePartialSearch : '',
  panel : ''
}
const reducer = (state = initialState, {type,payload}) => {
  switch(type){
    case 'PARTIAL_SEARCH':
      return {...state, partial : payload.data, valuePartialSearch: payload.value}
    case 'OPEN_PARTIAL_SEARCH_PANEL':
      return {...state, panel : payload.data, valuePartialSearch: payload.value}
    case 'VALUE_LENGTH_IS_0':
      return {...state, panel : payload.data, valuePartialSearch: payload.value}
    case 'RESET_VALUE':
      return {...state, valuePartialSearch: payload.value}
    default:
      return state;
  }
}

export default reducer;