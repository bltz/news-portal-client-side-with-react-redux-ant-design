const initialState = {
  fetched : false,
  indeks : [],
  rejectMsg : null
}

const reducer = (state = initialState, {type,payload}) => {
  switch(type){
    case 'FETCH_NASIONAL_FULFILLED':
      return {...state, indeks : payload.nasionalNews, fetched : true}
    case 'FETCH_NASIONAL_REJECTED':
      return {...state, rejectMsg : payload.nasionalNews, fetched : false}
    default:
      return state;
  }
}

export default reducer;