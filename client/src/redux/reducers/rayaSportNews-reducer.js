const initialState = {
  fetched : false,
  indeks : [],
  rejectMsg : null
}

const reducer = (state = initialState, {type,payload}) => {
  switch(type){
    case 'FETCH_RAYASPORT_FULFILLED':
      return {...state, indeks : payload.rayaSportNews, fetched : true}
    case 'FETCH_RAYASPORT_REJECTED':
      return {...state, rejectMsg : payload.rayaSportNews, fetched : false}
    default:
      return state;
  }
}

export default reducer;