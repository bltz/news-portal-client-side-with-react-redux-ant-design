const initialState = {
  fetched : false,
  indeks : [],
  rejectMsg : null
}

const reducer = (state = initialState, {type,payload}) => {
  switch(type){
    case 'FETCH_SPONSOREDNEWS_FULFILLED':
      return {...state, indeks : payload.sponsoredNews, fetched : true}
    case 'FETCH_SPONSOREDNEWS_REJECTED':
      return {...state, rejectMsg : payload.sponsoredNews, fetched : false}
    default:
      return state;
  }
}

export default reducer;