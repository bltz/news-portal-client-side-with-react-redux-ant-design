const initialState = {
  fetched : false,
  indeks : [],
  rejectMsg : null
}
const reducer = (state = initialState, {type,payload}) => {
  switch(type){
    case 'FETCH_HEADLINE_FULFILLED':
      return {...state, indeks : payload.headlineNews, fetched : true}
    case 'FETCH_HEADLINE_REJECTED':
      return {...state, rejectMsg : payload.headlineNews, fetched : false}
    default:
      return state;
  }
}

export default reducer;