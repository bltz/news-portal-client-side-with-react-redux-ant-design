const initialState = {
  fetched : false,
  indeks : [],
  rejectMsg : null
}

const reducer = (state = initialState, {type,payload}) => {
  switch(type){
    case 'FETCH_LASTESNEWS_FULFILLED':
      return {...state, indeks : payload.lastesNews, fetched : true}
    case 'FETCH_LASTESNEWS_REJECTED':
      return {...state, rejectMsg : payload.lastesNews, fetched : false }
    default:
      return state;
  }
}

export default reducer;