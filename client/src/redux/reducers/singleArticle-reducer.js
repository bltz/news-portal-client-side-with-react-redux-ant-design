const initialState = {
  reqSingleArticle : [],
  rejectSingleArticle : {
    response : '',
    findBySLug : ''
  },
  beritaTerkait : [],
  comment : []
}
const reducer = (state = initialState, {type,payload}) => {
  switch(type){
    case 'REQ_SINGLE_ARTICLE':
      return {...state, reqSingleArticle : payload.data, rejectSingleArticle:{
        response : '',
        findBySLug : ''
      }}
    case 'REJECT_REQ_SINGLE_ARTICLE':
      return {...state, reqSingleArticle:[], rejectSingleArticle : {
        response : payload.data,
        findBySLug : payload.slug
      }}
    default:
      return state;
  }
}

export default reducer;