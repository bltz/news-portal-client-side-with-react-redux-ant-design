const initialState = {
  fetched : false,
  indeks : [],
  rejectMsg : null
}

const reducer = (state = initialState, {type,payload}) => {
  switch(type){
    case 'FETCH_INTERNASIONAL_FULFILLED':
      return {...state, indeks : payload.internasionalNews, fetched : true}
    case 'FETCH_INTERNASIONAL_REJECTED':
      return {...state, rejectMsg : payload.internasionalNews, fetched : false}
    default:
      return state;
  }
}

export default reducer;