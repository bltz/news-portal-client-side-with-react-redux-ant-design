import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
import allReducers from './reducers';

const allStoreEnhancers = compose(
    applyMiddleware(thunk,createLogger()
  ),
  window.devToolsExtension && window.devToolsExtension() 
)

const store = createStore(
  allReducers,
  allStoreEnhancers
)

export default store