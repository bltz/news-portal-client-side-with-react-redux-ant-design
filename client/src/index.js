import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Home from './containers/Home';
import SingleArticle from './containers/SingleArticle'
import NotMatch404 from './containers/NotMatch404Component'
import registerServiceWorker from './registerServiceWorker';

import { Provider } from 'react-redux';
import store from './redux'

ReactDOM.render(
  <Provider store={store}>
    {/* <Home /> */}
    <BrowserRouter>
      <Switch>
        {/* <Route exact path='/' render={() => <Home/>}/> */}
        {/* <Route exact path='/404/:params' render={() => <NotMatch404/>}/> */}
        <Route exact path='/' component={Home}/>
        {/* <Route component={NotMatch404}/> */}
        <Route path='/:slug' component={SingleArticle}/>
        
      </Switch>
  </BrowserRouter>
  </Provider>
, document.getElementById('root'));
registerServiceWorker();
