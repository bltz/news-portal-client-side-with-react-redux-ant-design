import React from 'react'
import { Icon } from 'antd';
import Lazyload from 'react-lazy-load';
import moment from 'moment';
import 'moment/locale/id';
import ContentLoader from "react-content-loader";

class CardInMiddleVerOne extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      hover : false
    }
  }

  contentOnLoad(){
    return(
      <div className="card-v1-wraper">
        <ContentLoader
          height={300}
          width={540}
          speed={3}
          primaryColor="#f3f3f3"
          secondaryColor="#ecebeb"
        >
          <rect x="0" y="0" rx="0" ry="0" width="540" height="300" />
        </ContentLoader>
      </div>
    )
  }

  thumbCondition(dataIndek){
    let imgUrl = 'http://bourse-dechets-martinique.fr/wp-content/uploads/2014/10/no-image.png'
    let imageSource = ''
    if(dataIndek.thumbnail != null){
      imageSource = `http://localhost:3000/${dataIndek.thumbnail.path}`
    }else{
      imageSource = imgUrl
    }
    return imageSource
  }

  contentCondition(){
    const clearTagHtml = /(<([^>]+)>)/ig
    let resultClear = this.props.dataIndek.content.replace(clearTagHtml, "")
    return(
      <Lazyload throttle={20} height={300}>
        <div className="card-v1-wraper">
          <div className="cardv1-parent">
            <div className="cardv1-child"
              style={{backgroundImage:`url(${this.thumbCondition(this.props.dataIndek)})`}}
            >
              <a className="cardv1-headlines" href="">
              <span>
                <Icon type="paper-clip" style={{marginRight:'3px'}}/>{this.props.dataIndek.childCategoryID[0].name}
              </span>
              </a>
            </div>
          </div>
          <div className="card-v1-desc-wraper-container">
            <div className="card-v1-desc-wraper">
              <span className="card-v1-desc-categories">{this.props.dataIndek.parentCategoryID.name.toUpperCase()}</span>
              {this.props.dataIndek.uppercoupID ? <h6 className="card-v1-uppercoups">{this.props.dataIndek.uppercoupID.name}</h6> : <h6 className="card-v1-uppercoups" style={{display:'none'}}>...</h6>}
              
              <h2 className="card-v1-desc-title">{this.props.dataIndek.title.slice(0, 90)}</h2>
              <span className="card-v1-desc-date">{moment(this.props.dataIndek.createdAt).locale('id').format('dddd, Do MMMM YYYY- LT') + ' WIB'}</span>
            </div>
            <div className="card-v1-short-desc-wraper-container">
              <div className="card-v1-short-desc-wraper">
                { this.state.hover === true ?
                <div style={{height: '0px'}}></div> : <div style={{height: '30px'}}></div>
                }
                { this.state.hover === true ? 
                <div>
                  <div className="card-v1-inhover-divider"/>
                  <div className="card-v1-inhover-shortDesc">{this.props.dataIndek.content ? resultClear.slice(0,155)+' ...' : null}</div>
                  <a className="card-v1-inhover-readmore" href="">readmore</a>
                  <div className="card-v1-inhover-divider"/>
                </div> : <div style={{display:'none'}}/>
                }
              </div>
            </div>
          </div>
        </div>
      </Lazyload>
    )
  }
  
  render(){
    return(
      <div className="card-v1-wraper-container"
      onMouseEnter={()=>this.setState({hover:true})}
      onMouseLeave={()=>this.setState({hover:false})}
      >
      {this.props.dataIndek ? this.contentCondition() : this.contentOnLoad()}
      </div>
    )
  }
}

export default CardInMiddleVerOne