import React from 'react';
import { Avatar, Badge, Icon, message } from 'antd';
import UserLoginButton from './UserLoginButtonComponent'

const success = () => {
  message.success('Logout Berhasil', 5);
  localStorage.clear()
  // window.location.assign("/")
};

class UserActiveButton extends React.Component{
  constructor() {
    super();
    this.state = {
      userSubMenu : false
    };
  }
  handleLogout(){
    success()
    // setTimeout(success(), 1500);
    // window.location.assign("/")
    // localStorage.clear()
  }
  menuUsers(){
    return(
      <div className="submenu-users font-global-form-login" 
        onMouseLeave={()=>{this.setState({userSubMenu:false})}}
      >
        <ul>
          <li style={{pointerEvents: 'none', color:'grey'}}>
            <Icon type="dashboard" style={{float: 'left',margin:'3px 6px 0 0', color:'#e8e8e8', fontWeight:'100'}}/>
            <a style={{color:'#e8e8e8'}} title={`Dashboard`} aria-label={`Dashboard`} className="font-global-form-login">Dashboard</a>
          </li>
          <li style={{pointerEvents: 'none', color:'#e8e8e8'}}>
            <Icon type="smile-o" style={{float: 'left',margin:'3px 6px 0 0', color:'#e8e8e8', fontWeight:'100'}}/>
            <a style={{color:'#e8e8e8'}} title={`Profile Kamu`} aria-label={`Profile Kamu`}>Profile Kamu</a>
          </li>
          <li style={{pointerEvents: 'none', color:'#e8e8e8'}}>
            <Icon type="solution" style={{float: 'left',margin:'3px 6px 0 0', color:'#e8e8e8', fontWeight:'100'}}/>
            <a style={{color:'#e8e8e8'}} title={`Edit Profile`} aria-label={`Edit Profile`}>Edit Profile</a>
          </li>
          <li>
            <Icon type="logout" style={{float: 'left',margin:'3px 6px 0 0', color:'black', fontWeight:'100'}}/>
            <a onClick={this.handleLogout} title={`Logout`} aria-label={`Logout`}>Logout</a>
          </li>
        </ul>
      </div>
    )
  }
  handleMenuUsers = (e) => {
    if(this.state.userSubMenu === false){
      this.setState({userSubMenu:true})
    }else{
      this.setState({userSubMenu:false})
    }
  }
  avatarCondition(avatar, name){
    if(avatar){
      return(
        <div>
          <div className='icon-login'>
            <Badge dot style={{ margin: '17px 5px 0 0'}}>
              <Avatar style={{ marginTop: '17px', fontSize: 28, color: 'rgba(0, 0, 0, 0.85)'}} 
                src={avatar} 
              />
            </Badge>
          </div>
          <div className='title-login-wraper'>
            <span className="title-login">{name}</span>
            <span className="desc-title-login">Community Users</span>
          </div>
        </div>
      )
    }else if(name){
      return(
        <div>
          <div className='icon-login'>
            <Badge dot style={{ margin: '17px 5px 0 0'}}>
              <Avatar style={{ margin: '17px 1px 0 0', backgroundColor: '#87d068' }}>{name.slice(0,1).toUpperCase()}</Avatar>
            </Badge>
          </div>
          <div className='title-login-wraper'>
            <span className="title-login">{name}</span>
            <span className="desc-title-login">Community Users</span>
          </div>
        </div>
      )
    }else{
      return(
        <UserLoginButton/>
      )
    }
  }
  render(){
    let name = localStorage.getItem('fullname')
    return(
      <div className='header-account' onClick={this.handleMenuUsers}>
        <a title="Reza Aditya" aria-label="Reza Aditya">
          {/* <div className='icon-login'> */}
          {this.avatarCondition(localStorage.getItem('avatar'), name)}
          {/* </div>
          <div className='title-login-wraper'>
            <span className="title-login">{name}</span>
            <span className="desc-title-login">Community Users</span>
          </div> */}
        </a>
        {this.state.userSubMenu === true ? this.menuUsers() : <div style={{display:'none'}}/>}
      </div>
    )
  }
}


export default UserActiveButton
