import React from 'react';
import { Icon } from 'antd';

class UserLoginButton extends React.Component{
  render(){
    const styles = {
      spanWraper : {
        fontSize: '14px',
        lineHeight: '1.7',
        color: 'black',
        margin: '0px',
        transition: 'all 0.4s',
        WebkitTransition: 'all 0.4s',
        OTransition: 'all 0.4s',
        MozTransition: 'all 0.4s',
        cursor: 'pointer'
      }
    }
    return(
      <div className='header-account' onClick={this.props.openModal}>
        <span style={styles.spanWraper}>
          <div className='icon-login'>
          <Icon type="user" style={{ marginTop: '17px', fontSize: 28, color: 'rgba(0, 0, 0, 0.85)' }} />
          </div>
          <div className='title-login-wraper'>
            <span className="title-login">Login / Register</span>
            <span className="desc-title-login">create your article</span>
          </div>
        </span>
      </div>
    )
  }
}

export default UserLoginButton