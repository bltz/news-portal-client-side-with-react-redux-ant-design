import React from 'react'
import ContentLoader from "react-content-loader";

class CardInRightVerOne extends React.Component{
  contentOnLoad(){
    return(
      <div>
        <ContentLoader
          height={307}
          width={300}
          speed={3}
          primaryColor="#dddfe1"
          secondaryColor="#ecebeb"
        >
          <rect x="2" y="233" rx="0" ry="0" width="72.1" height="10.1" /> 
          <rect x="84.52" y="233" rx="0" ry="0" width="103" height="10.1" /> 
          <rect x="197.98" y="233" rx="0" ry="0" width="10.3" height="10.1" /> 
          <rect x="17.47" y="253.29" rx="0" ry="0" width="133.9" height="10.1" /> 
          <rect x="161.88" y="253.29" rx="0" ry="0" width="133.9" height="10.1" /> 
          <rect x="17.47" y="273.59" rx="0" ry="0" width="92.7" height="10.1" /> 
          <rect x="120.62" y="273.59" rx="0" ry="0" width="61.8" height="10.1" /> 
          <rect x="192.82" y="273.59" rx="0" ry="0" width="61.8" height="10.1" /> 
          <rect x="2" y="293.88" rx="0" ry="0" width="30.9" height="10.1" /> 
          <rect x="3" y="197.05" rx="0" ry="0" width="109" height="19" /> 
          <rect x="120" y="197.05" rx="0" ry="0" width="164" height="19" /> 
          <rect x="0" y="0" rx="0" ry="0" width="300" height="180" />
        </ContentLoader>
      </div>
    )
  }

  thumbCondition(dataIndek){
    let imgUrl = 'http://bourse-dechets-martinique.fr/wp-content/uploads/2014/10/no-image.png'
    let imageSource = ''
    if(dataIndek.thumbnail != null){
      imageSource = `http://localhost:3000/${dataIndek.thumbnail.path}`
    }else{
      imageSource = imgUrl
    }
    return imageSource
  }

  contentCondition(){
    const clearTagHtml = /(<([^>]+)>)/ig
    let resultClear = this.props.dataIndeks.indeks[0].content.replace(clearTagHtml, "")
    return (
      <a 
        target="_blank" 
        rel="nofollow" 
        href={`/${this.props.dataIndeks.indeks[0].slug}`}
        title={`${this.props.dataIndeks.indeks[0].title}`} 
        aria-label={`${this.props.dataIndeks.indeks[0].title}`} 
      >
        <div className="img-advertise-wraper">
          <div className="img-advertise-parent">
            <div className="img-advertise-child"
              style={{backgroundImage:`url(${this.thumbCondition(this.props.dataIndeks.indeks[0])})`}}
            />
          </div>
        </div>
        <h5 className="title-advertise">{this.props.dataIndeks.indeks[0].title}</h5>
        <p className="desc-advertise">{this.props.dataIndeks.indeks[0].content ? resultClear.slice(0,155)+' ...' : null}</p>
      </a>
    )
  }
  render(){
    return(
      <div className="card-sponsored">
        {this.props.dataIndeks.fetched === true ? this.contentCondition() : this.contentOnLoad()}
      </div>
    )
  }
}

export default CardInRightVerOne