import React from 'react';
import { Row, Col, Breadcrumb, Icon, Divider } from 'antd';
import { Link } from 'react-router-dom';
import StickyNode from 'react-stickynode'
import RightSidebarVerOne from './RightSidebarVerOneComponent'
import HeadIndex from './HeadIndexComponent'
import ImageZoom from 'react-medium-image-zoom'
import ReactHtmlParser from 'react-html-parser'
import moment from 'moment';
import 'moment/locale/id';
import ImageGallery from 'react-image-gallery';
// import NotMatch404 from '../containers/NotMatch404Component'

class ContentArticleDetail extends React.Component{
  thumbCondition(dataIndek){
    let imgUrlNoImage = 'http://bourse-dechets-martinique.fr/wp-content/uploads/2014/10/no-image.png'
    if(dataIndek.thumbnail != null){
      return (
        <div>
          <div className="post-feat-text">
            <span className="post-excerpt" style={{float:'left'}}>
              <p >How to change jobs — or handle a layoff — and end up happier, richer, less stressed and better off</p>
            </span>
            <span className="feat-caption">Photo: Shutterstock</span>
          </div>
          <ImageZoom
            image={{
              src: `http://localhost:3000/${dataIndek.thumbnail.path}`,
              alt: 'Golden Gate Bridge',
              className: 'img-thumbnail-child',
              style: { width: '100%' }
            }}
            zoomImage={{
              src: `http://localhost:3000/${dataIndek.thumbnail.path}`,
              alt: 'Golden Gate Bridge'
            }}
          />
        </div>
      )
    }else{
      return (
        <img src={imgUrlNoImage} className="img-thumbnail-child" alt='no-thumb'/>
      )
    }
  }
  thumbImg(response){
    return (
      <div className="img-thumbnail">
        <div className="img-thumbnail-parent">
        {this.thumbCondition(response)}
        </div>
      </div>
    )
  }
  gallery(response){
    const images = []
    response.galleryID.imagesID.map(image => {
      return images.push({
        original: `http://localhost:3000/${image.path}`,
        thumbnail: `http://localhost:3000/${image.path}`,
        description: <h5>{image.description} <br/><span style={{fontSize: '11px'}}>ImageBy: {image.caption}</span></h5>,
        originalTitle: `${image.title}`
      })
    })
    return(
      <div className="img-thumbnail">
        <div className="img-thumbnail-parent">
          <ImageGallery
            items={images}
            lazyLoad={true} 
            showThumbnails={true}
            showIndex={true}
            autoPlay={true}
            showPlayButton={true}
            slideOnThumbnailHover={true}
            slideInterval={5000}
          />
        </div>
    </div>
    )
  }
  tagCondition(response){
    return (
      response.tagID.map(tag => {
        return (
          <Link to='/' key={tag._id}><span className="tag-items">#{tag.name}</span></Link>
        )
      })
    )
  }
  contentOnLoad(message, response){
    if(message === "finded by slug News"){
      return(
        <div className="content-wraper font-global-form-login" style={{marginBottom: '18px'}}>
          <Row>
            <Col span={18} className="left-bar-detail-article">
              <div>
                <Breadcrumb style={{fontSize:'11px',fontWeight:'500'}}>
                  <Breadcrumb.Item><Link to="/" style={{fontSize:'11px',fontWeight:'600'}}>HOME</Link></Breadcrumb.Item>
                  <Breadcrumb.Item><Link to="/nasional" style={{fontSize:'11px',fontWeight:'600'}}>{this.props.response.parentCategoryID.name.toUpperCase()}</Link></Breadcrumb.Item>
                  <Breadcrumb.Item><Link to="/politik" style={{fontSize:'11px',fontWeight:'600'}}>{this.props.response.childCategoryID[0].name.toUpperCase()}</Link></Breadcrumb.Item>
                  <Breadcrumb.Item>{this.props.response.title.slice(0,66)+' ...'}</Breadcrumb.Item>
                </Breadcrumb>  
              </div>
              <div className="article-detail-title-wraper">
                <div className="categories">{this.props.response.childCategoryID[0].name.toUpperCase()}</div>
                <h5 className="uppercoups">{this.props.response.uppercoupID ? this.props.response.uppercoupID.name : null}</h5>
                <h1>{this.props.response.title}</h1>
                <h6>{moment(this.props.response.createdAt).locale('id').format('dddd, Do MMMM YYYY- LT') + ' WIB'}</h6>
              </div>

              {response.typeNews === "galleryArticle" ? this.gallery(this.props.response) : this.thumbImg(this.props.response)}
              
              <div className="article-detail">
                {ReactHtmlParser(this.props.response.content)}
              </div>
              <div className="share-container">
                <Divider className="divider-detail-article"/>
                <div className="share-wrap">
                  <span className="akumulasi-share">
                    <span style={{fontWeight:'800',margin:'0 5px 0 0'}}>120K</span>SHARE
                  </span>
                  <span className="sosmed-share">
                    <button>
                      <Icon type="facebook" style={{fontSize:'30px',verticalAlign: 'middle', marginRight: '10px'}}/> 
                      <span className="share-count">280</span>
                    </button>
                  </span>
                  <Divider type="vertical" style={{height:'30px',width:'1px',background:'#5a5a5a'}}/>
                  <span className="sosmed-share">
                    <button>
                      <Icon type="twitter" style={{fontSize:'30px',verticalAlign: 'middle', marginRight: '10px'}}/> 
                      <span className="share-count">10K</span>
                    </button>
                  </span>
                  <Divider type="vertical" style={{height:'30px',width:'1px',background:'#5a5a5a'}}/>
                  <span className="sosmed-share">
                    <button>
                      <Icon type="google-plus" style={{fontSize:'30px',verticalAlign:'middle',marginRight:'10px'}}/> 
                      <span className="share-count">999</span>
                    </button>
                  </span>
                </div>
                <Divider className="divider-detail-article"/>
                <div>
                  <span className="tag-head">TAG</span>
                  {this.tagCondition(this.props.response)}
                </div>
                <Divider className="divider-detail-article"/>
              </div>
              <HeadIndex title='berita terkait' padding='8px 10px'/>
            </Col>
            <Col span={6} className="right-bar-index">
              <StickyNode 
                enabled={true}
                top={120}
                innerZ={2}
                bottomBoundary={3070+this.props.plusHeight}
              >
                <RightSidebarVerOne/>
              </StickyNode>
            </Col>
          </Row>
        </div>
      )
    }else{
      // window.location.assign(`/404/paramsss`);
      // return <div>err</div> 
      // return <NotMatch404/>
    }
  }
  render(){
    return(
      <div>
        {/* {console.log(this.props.response,'::::')} */}
        {/* {this.props.response === "::: NO DATA SINGLE NEWS BY SLUG :::" ? <div>err</div>} */}
      {this.contentOnLoad(this.props.message, this.props.response)}
      </div>
    )
  }
}

export default ContentArticleDetail