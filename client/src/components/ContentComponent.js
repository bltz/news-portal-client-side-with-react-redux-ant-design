import React from 'react';
import { Row, Col } from 'antd';
import StickyNode from 'react-stickynode'
import MiddleTerkini from './MiddleTerkiniComponent'
import LeftSidebar from './LeftSidebarComponent'
import RightSidebarVerOne from './RightSidebarVerOneComponent'

class Content extends React.Component{
  render(){
    return(
      <div className="content-wraper">
        <Row>
          <Col span={6} className="left-bar-index">
            <StickyNode 
              enabled={true}
              top={120}
              innerZ={2}
              bottomBoundary={3070+this.props.plusHeight}
            >
              <LeftSidebar/>
            </StickyNode>
          </Col>
          <Col span={12} className="middle-bar-index">
            <MiddleTerkini plusHeight={this.props.plusHeight}/> 
          </Col>
          <Col span={6} className="right-bar-index">
            <StickyNode 
              enabled={true}
              top={120}
              innerZ={2}
              bottomBoundary={3070+this.props.plusHeight}
            >
              <RightSidebarVerOne/>
            </StickyNode>
          </Col>
        </Row>
      </div>
    )
  }
}

export default Content