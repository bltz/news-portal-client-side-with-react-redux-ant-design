import React from 'react';

class InlineRectangleAdv extends React.Component{
  render(){
    return(
      <div className="img-banner-advertise">
        <a 
          target="_blank" 
          rel="nofollow" 
          href="/"
          title={`inline adv`} 
          aria-label={`inline adv`} 
        >
          <img className="img-advertise" alt="landing-pages" src="https://s3.amazonaws.com/files.bannersnack.net/site/images/landingPages/gallery/bcma6f3jo"/>
        </a>
      </div>
    )
  }
}

export default InlineRectangleAdv