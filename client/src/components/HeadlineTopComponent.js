import React from 'react';
import { Row, Col, Carousel, Icon } from 'antd';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import moment from 'moment';
import 'moment/locale/id';
import ContentLoader from "react-content-loader";

class HeadlineTop extends React.Component{
  contentOnLoad(){
    return(
      <Row>
        <Col span={15} className="firstbox-col" style={{padding: '0 5px 0 0'}}>
          <ContentLoader
            height={500}
            width={731.25}
            speed={2}
            primaryColor="#dddfe1"
		        secondaryColor="#ecebeb"
          >
            <rect x="0" y="0" rx="0" ry="0" width="731.25" height="500" />
          </ContentLoader>
        </Col>
        <Col span={9} className="secondbox-col">
          <ContentLoader
            height={250}
            width={438.75}
            speed={3}
            primaryColor="#dddfe1"
		        secondaryColor="#ecebeb"
          >
            <rect x="0" y="0" rx="0" ry="0" width="438.75" height="245" />
          </ContentLoader>
        </Col>
        <Col span={9} className="secondbox-col">
          <ContentLoader
            height={250}
            width={438.75}
            speed={1.5}
            primaryColor="#dddfe1"
		        secondaryColor="#ecebeb"
          >
            <rect x="0" y="0" rx="0" ry="0" width="438.75" height="250" />
          </ContentLoader>
        </Col>
      </Row>
    )
  }
  
  thumbCondition(dataIndek){
    let imgUrl = 'http://bourse-dechets-martinique.fr/wp-content/uploads/2014/10/no-image.png'
    let imageSource = ''
    if(dataIndek.thumbnail != null){
      imageSource = `http://localhost:3000/${dataIndek.thumbnail.path}`
    }else{
      imageSource = imgUrl
    }
    return imageSource
  }

  contentCondition(settings){
    return(
      <Row>
        <Col span={15} className="firstbox-col">
        <div className="parent">
            <div className="child"
              style={{backgroundImage:`url(${this.thumbCondition(this.props.topHeadlineNews.indeks[0])})`}}
            >
              <a className="headlines" href="">
              <span><Icon type="paper-clip" style={{marginRight:'3px'}}/>Headline</span>
              </a>
            </div>
        </div>
          <div className="firstbox-caption-wraper">
            <p className="firstbox-caption-categories">{this.props.topHeadlineNews.indeks[0].parentCategoryID.name.toUpperCase()}</p>
            <Link to={`/${this.props.topHeadlineNews.indeks[0].slug}`}><h1 className="firstbox-caption-title">{this.props.topHeadlineNews.indeks[0].title}</h1></Link>
            <span className="firstbox-caption-date">{moment(this.props.topHeadlineNews.indeks[0].createdAt).locale('id').format('dddd, Do MMMM YYYY- LT') + ' WIB'}</span>
          </div>
        </Col>
        <Col span={9} className="secondbox-col">
        <div className="secondbox-img">
            <div className="secondbox-img-child"
              style={{backgroundImage:`url(${this.thumbCondition(this.props.topHeadlineNews.indeks[1])})`}}
            >
              <a className="headlines" href="">
                <span><Icon type="paper-clip" style={{marginRight:'3px'}}/>Headline</span>
              </a>
            </div>
        </div>
          <div className="secondbox-caption-wraper">
            <span className="secondbox-caption-categories">{this.props.topHeadlineNews.indeks[1].parentCategoryID.name.toUpperCase()}</span>
            <Link to={`/${this.props.topHeadlineNews.indeks[1].slug}`}><h3 className="secondbox-caption-title">{this.props.topHeadlineNews.indeks[1].title}</h3></Link>
            <span className="secondbox-caption-date">{moment(this.props.topHeadlineNews.indeks[1].createdAt).locale('id').format('dddd, Do MMMM YYYY- LT') + ' WIB'}</span>
          </div>
        </Col>
        <Col span={9} className="secondbox-col">
          <Carousel {...settings}>
            <div>
              <div className="secondbox-img">
                <div className="secondbox-img-child"
                  style={{backgroundImage:`url(${this.thumbCondition(this.props.topHeadlineNews.indeks[2])})`}}
                >
                <a className="headlines" href="">
                  <span><Icon type="paper-clip" style={{marginRight:'3px'}}/>Headline</span>
                  </a>
                </div>
              </div>
              <div style={{backgroundColor: 'rgba(0, 0, 0, 0.33)'}} className='headlineSlider'>
                <span className='parentCategoriesSlider'>{this.props.topHeadlineNews.indeks[2].parentCategoryID.name.toUpperCase()}</span>
                <Link to={`/${this.props.topHeadlineNews.indeks[2].slug}`}><h3>{this.props.topHeadlineNews.indeks[2].title}</h3></Link>
                <div className='dateSlider'>{moment(this.props.topHeadlineNews.indeks[2].createdAt).locale('id').format('dddd, Do MMMM YYYY- LT') + ' WIB'}</div>
              </div>  
            </div>
            <div>
              <div className="secondbox-img">
                <div className="secondbox-img-child"
                  style={{backgroundImage:`url(${this.thumbCondition(this.props.topHeadlineNews.indeks[3])})`}}
                >
                  <a className="headlines" href="">
                    <span><Icon type="paper-clip" style={{marginRight:'3px'}}/>Headline</span>
                  </a>
                </div>
              </div>
              <div className='headlineSlider'>
                <span className='parentCategoriesSlider'>{this.props.topHeadlineNews.indeks[3].parentCategoryID.name.toUpperCase()}</span>
                <Link to={`/${this.props.topHeadlineNews.indeks[3].slug}`}><h3>{this.props.topHeadlineNews.indeks[3].title}</h3></Link>
                <div className='dateSlider'>{moment(this.props.topHeadlineNews.indeks[3].createdAt).locale('id').format('dddd, Do MMMM YYYY- LT') + ' WIB'}</div>
              </div>  
            </div>
            <div>
            <div className="secondbox-img">
              <div className="secondbox-img-child"
                style={{backgroundImage:`url(${this.thumbCondition(this.props.topHeadlineNews.indeks[4])})`}}
              >
                <a className="headlines" href="">
                  <span><Icon type="paper-clip" style={{marginRight:'3px'}}/>Headline</span>
                </a>
              </div>
            </div>
              <div className='headlineSlider'>
                <span className='parentCategoriesSlider'>{this.props.topHeadlineNews.indeks[4].parentCategoryID.name.toUpperCase()}</span>
                <Link to={`/${this.props.topHeadlineNews.indeks[4].slug}`}><h3>{this.props.topHeadlineNews.indeks[4].title}</h3></Link>
                <div className='dateSlider'>{moment(this.props.topHeadlineNews.indeks[4].createdAt).locale('id').format('dddd, Do MMMM YYYY- LT') + ' WIB'}</div>
              </div>  
            </div>
            <div>
              <div className="secondbox-img">
                <div className="secondbox-img-child"
                  style={{backgroundImage:`url(${this.thumbCondition(this.props.topHeadlineNews.indeks[5])})`}}
                >
                  <a className="headlines" href="">
                    <span><Icon type="paper-clip" style={{marginRight:'3px'}}/>Headline</span>
                  </a>
                </div>
              </div>
              <div className='headlineSlider'>
                <span className='parentCategoriesSlider'>{this.props.topHeadlineNews.indeks[5].parentCategoryID.name.toUpperCase()}</span>
                <Link to={`/${this.props.topHeadlineNews.indeks[5].slug}`}><h3>{this.props.topHeadlineNews.indeks[5].title}</h3></Link>
                <div className='dateSlider'>{moment(this.props.topHeadlineNews.indeks[5].createdAt).locale('id').format('dddd, Do MMMM YYYY- LT') + ' WIB'}</div>
              </div>  
            </div>
          </Carousel>
        </Col>
      </Row>
    )
  }

  fetchHeadlineNews(settings){
    if(this.props.topHeadlineNews.fetched === true){
      return this.props.topHeadlineNews.indeks.length === 6 ? this.contentCondition(settings) : this.contentOnLoad()
    }else{
      return this.contentOnLoad()
    }
  }

  render(){
    const settings = {
      dots: true,
      lazyLoad: true,
      infinite: true,
      speed: 500,
      // slidesToShow: 1,
      // slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 5000,
      cssEase: "linear",
      // swipeToSlide: true,
      pauseOnHover: true,
      afterChange: function(index) {
        // console.log(
        //   `Slider Changed to: ${index + 1}, background: #222; color: #bada55`
        // );
      }
    };
    return(
      <div className="headline-wraper">
        {this.fetchHeadlineNews(settings)}
      </div>
    )
  }
}

const mapStateToProps = (state,props) => {
  return{
    topHeadlineNews : state.headlineNews
  }
}

export default connect(
  mapStateToProps
)(HeadlineTop)