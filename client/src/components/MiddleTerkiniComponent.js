import React from 'react';
import HeadIndex from './HeadIndexComponent'
import StickyNode from 'react-stickynode'
import CardInMiddleVerOne from './CardInMiddleVerOneComponent'
import CardInMiddleVerTwo from './CardInMiddleVerTwoComponent'
import ListTitleBeforeCardVerTwo from './ListTitleBeforeCardVerTwoComponent'
import OnloadCardMiddleVerOne from './onLoadContent/OnLoadCardMiddleVerOneComponent'
import OnLoadCardMiddleVerTwo from './onLoadContent/OnLoadCardMiddleVerTwoComponent'

import { connect } from 'react-redux'

class MiddleTerkini extends React.Component{
  render(){
    const styles = {
      blockListCardVerTwo : {
        display: 'flex',
        WebkitBoxPack: 'justify',
        justifyContent: 'space-between',
        marginTop: '-3px'
      }
    }
    return(
      <div>
        <StickyNode 
          enabled={true}
          innerZ={2}
          top={120}
          bottomBoundary={1207+this.props.plusHeight}
        >
          <HeadIndex title='berita terkini' padding='5px 10px'/>
        </StickyNode>
        {this.props.lastestNewsIndex.fetched === true ? <CardInMiddleVerOne dataIndek={this.props.lastestNewsIndex.indeks[0]}/> : <OnloadCardMiddleVerOne/>}
        <div style={styles.blockListCardVerTwo}>
        {console.log(this.props.lastestNewsIndex,'>?>?')}
          {this.props.lastestNewsIndex.fetched === true ? <CardInMiddleVerTwo widthStyle='19%' dataIndek={this.props.lastestNewsIndex.indeks[1]}/> : <OnLoadCardMiddleVerTwo widthStyle='19%'/>}
          {this.props.lastestNewsIndex.fetched === true ? <CardInMiddleVerTwo widthStyle='19%' dataIndek={this.props.lastestNewsIndex.indeks[2]}/> : <OnLoadCardMiddleVerTwo widthStyle='19%'/>}
          {this.props.lastestNewsIndex.fetched === true ? <CardInMiddleVerTwo widthStyle='19%' dataIndek={this.props.lastestNewsIndex.indeks[3]}/> : <OnLoadCardMiddleVerTwo widthStyle='19%'/>}
          {this.props.lastestNewsIndex.fetched === true ? <CardInMiddleVerTwo widthStyle='19%' dataIndek={this.props.lastestNewsIndex.indeks[4]}/> : <OnLoadCardMiddleVerTwo widthStyle='19%'/>}
          {this.props.lastestNewsIndex.fetched === true ? <CardInMiddleVerTwo widthStyle='19%' dataIndek={this.props.lastestNewsIndex.indeks[5]}/> : <OnLoadCardMiddleVerTwo widthStyle='19%'/>}
        </div>
        <StickyNode 
          enabled={true}
          innerZ={2}
          top={120}
          bottomBoundary={1690+this.props.plusHeight}
        >
          <HeadIndex title='nasional' padding='5px 10px'/>
        </StickyNode>
        {this.props.nasionalNewsIndex.fetched === true ? <CardInMiddleVerOne dataIndek={this.props.nasionalNewsIndex.indeks[0]}/> : <OnloadCardMiddleVerOne/>}
        <ListTitleBeforeCardVerTwo listTitles={this.props.nasionalNewsIndex}/>
        <StickyNode 
          enabled={true}
          innerZ={2}
          top={120}
          bottomBoundary={2180+this.props.plusHeight}
        >
          <HeadIndex title='internasional' padding='5px 10px'/>
        </StickyNode>
        {this.props.internasionalNewsIndex.fetched === true ? <CardInMiddleVerOne dataIndek={this.props.internasionalNewsIndex.indeks[0]}/> : <OnloadCardMiddleVerOne/>}
        <ListTitleBeforeCardVerTwo listTitles={this.props.internasionalNewsIndex}/>
        <StickyNode 
          enabled={true}
          innerZ={2}
          top={120}
          bottomBoundary={2650+this.props.plusHeight}
        >
          <HeadIndex title='raya daerah' padding='5px 10px'/>
        </StickyNode>
        {this.props.rayaDaerahNewsIndex.fetched === true ? <CardInMiddleVerOne dataIndek={this.props.rayaDaerahNewsIndex.indeks[0]}/> : <OnloadCardMiddleVerOne/>}
        <ListTitleBeforeCardVerTwo listTitles={this.props.rayaDaerahNewsIndex}/>
        <StickyNode 
          enabled={true}
          innerZ={2}
          top={120}
          bottomBoundary={3070+this.props.plusHeight}
        >
          <HeadIndex title='raya pop' padding='5px 10px'/>
        </StickyNode>
        {this.props.rayaPopNewsIndex.fetched === true ? <CardInMiddleVerOne dataIndek={this.props.rayaPopNewsIndex.indeks[0]}/> : <OnloadCardMiddleVerOne/>}
        <ListTitleBeforeCardVerTwo listTitles={this.props.rayaPopNewsIndex}/>
      </div>
    )
  }
}

const mapStateToProps = (state,props) => {
  return {
    lastestNewsIndex : state.lastesNews,
    nasionalNewsIndex : state.nasionalNews,
    internasionalNewsIndex : state.internasionalNews,
    rayaDaerahNewsIndex : state.rayaDaerahNews,
    rayaPopNewsIndex : state.rayaPopNews
  }
}

export default connect(
  mapStateToProps
)(MiddleTerkini);
