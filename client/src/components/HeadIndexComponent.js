import React from 'react';
import { Icon } from 'antd';

class HeadIndex extends React.Component{
  render(){
    const styles = {
      paddingWrap : {
        padding : this.props.padding
      }
    }
    return(
      <div className="head-title-kanal-wraper" style={styles.paddingWrap}>
        <div className="title-kanal">
          {this.props.title.toUpperCase()}</div>
          <a 
            title={`Indeks ${this.props.title}`} 
            aria-label={`Indeks ${this.props.title}`} 
            className="icon-index"
            style={{color:'#9a201d'}}
          >
            <Icon type="plus-circle-o" style={{ fontWeight:'bold', cursor: 'pointer' }}/>
          </a>
      </div>
    )
  }
}

export default HeadIndex