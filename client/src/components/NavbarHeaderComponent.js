import React from 'react'
import { Icon, Menu } from 'antd';
import NavbarHeaderSubMenuLainnya from './NavbarHeaderSubMenuLainnyaComponent'
import RayaChannel from './subMenuNavBar/RayaChannel'
import RayaTrip from './subMenuNavBar/RayaTrip'
import RayaOto from './subMenuNavBar/RayaOto'
import RayaSport from './subMenuNavBar/RayaSport'
import RayaPop from './subMenuNavBar/RayaPop'
import RayaDaerah from './subMenuNavBar/RayaDaerah'
import Internasional from './subMenuNavBar/Internasional'
import Nasional from './subMenuNavBar/Nasional'

class NavbarHeader extends React.Component{
  state = {
    current: 'menu',
    subMenuLainnya : false,
    hover: ''
  }
  handleClick = (e) => {
    console.log('click ', e.key);
    this.setState({
      current: e.key,
    });
  }
  handleHover = (e) => {
    console.log('hover ', e.target.value);
  }
  closeUpHover(){
    if(this.state.hover !== ''){
      // this.setState({hover:''})
    }
  }
  handlehoverNav(){
    if(this.state.hover === 'rayachannel'){
      return(<div onMouseLeave={()=>{this.setState({hover:''})}}><RayaChannel/></div>)
    }else if(this.state.hover === 'rayatrip'){
      return(<div  onMouseLeave={()=>{this.setState({hover:''})}}><RayaTrip/></div>)
    }else if(this.state.hover === 'rayaoto'){
      return(<div  onMouseLeave={()=>{this.setState({hover:''})}}><RayaOto/></div>)
    }else if(this.state.hover === 'rayasport'){
      return(<div  onMouseLeave={()=>{this.setState({hover:''})}}><RayaSport/></div>)
    }else if(this.state.hover === 'rayapop'){
      return(<div  onMouseLeave={()=>{this.setState({hover:''})}}><RayaPop/></div>)
    }else if(this.state.hover === 'rayadaerah'){
      return(<div  onMouseLeave={()=>{this.setState({hover:''})}}><RayaDaerah/></div>)
    }else if(this.state.hover === 'internasional'){
      return(<div  onMouseLeave={()=>{this.setState({hover:''})}}><Internasional/></div>)
    }else if(this.state.hover === 'nasional'){
      return(<div  onMouseLeave={()=>{this.setState({hover:''})}}><Nasional/></div>)
    }
  }
  handleLainnya(){
    if(this.state.subMenuLainnya === true){
      return(
        <Menu.Item key="close" onClick={()=>{this.setState({subMenuLainnya:false})}} style={{zIndex:'2',float: 'right',background:'#272727'}}>
        <div style={{float:'right'}}>Close
          <Icon key="close" 
            type="close"
            style={{fontSize:'13px',color:'red',fontWeight:'bold',marginLeft:'5px'}} 
          />
        </div>
        </Menu.Item>
      )
    }else{
      return(
        <Menu.Item key="lainnya" onClick={()=>{this.setState({subMenuLainnya:true,hover:''})}} style={{float: 'right'}}>
          <div>More <Icon type="down" style={{marginLeft:'5px'}}/></div>
        </Menu.Item>
      )
    }
  }
  
  subMenu(styles){
    return(
      <NavbarHeaderSubMenuLainnya/>
    )
  }

  render(){
    const styles = {
      hotBadge : {
        backgroundColor: '#f5222d',
        lineHeight: '12px',
        fontSize: '9px',
        marginTop: '-13px',
        padding: '2px 7px',
        zIndex: '1'
      }
    }
    return(
      <div>
        <Menu
          theme="dark"
          onClick={this.handleClick}
          selectedKeys={[this.state.current]}
          mode="horizontal"
          defaultSelectedKeys={['1']}
          style={{ lineHeight: '40px', background:'black' }}
        >
          <Menu.Item key="menu" style={{background:'red'}}>
            <Icon type="menu-unfold" style={{ fontSize: 15, margin:'0' }} />
          </Menu.Item>
          <Menu.Item key="nasional"
            onMouseOver={() => {this.setState({hover:'nasional'})}}
          >NASIONAL</Menu.Item>
          <Menu.Item key="internasional"
            onMouseOver={() => {this.setState({hover:'internasional'})}}
          >INTERNASIONAL</Menu.Item>
          <Menu.Item key="rayadaerah"
            onMouseOver={() => {this.setState({hover:'rayadaerah'})}}
          >RAYA DAERAH</Menu.Item>
          <Menu.Item key="rayapop"
            onMouseOver={() => {this.setState({hover:'rayapop'})}}
          >RAYA POP</Menu.Item>
          <Menu.Item key="rayasport"
            onMouseOver={() => {this.setState({hover:'rayasport'})}}
          >RAYA SPORT</Menu.Item>
          <Menu.Item key="rayaoto"
            onMouseOver={() => {this.setState({hover:'rayaoto'})}}
          >RAYA OTO</Menu.Item>
          <Menu.Item key="rayatrip"
            onMouseOver={() => {this.setState({hover:'rayatrip'})}}
          >
            RAYA TRIP
          </Menu.Item>
          <Menu.Item key="rayachannel" 
            onMouseLeave={this.closeUpHover()}
            onMouseOver={() => {this.setState({hover:'rayachannel'})}}
          >
            RAYA CHANNEL
            <span className="badge" style={styles.hotBadge}>Hot</span>
          </Menu.Item>
          {this.handleLainnya()}
        </Menu>
        {this.handlehoverNav()}
        {this.state.subMenuLainnya === true ? this.subMenu(styles) : <div style={{display:'none'}}/>}
      </div>
    )
  }
}

export default NavbarHeader