import React from 'react';
import { Col, Row, Badge, Icon } from 'antd';
import TextLoop from 'react-text-loop';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux'

class NavbarHeaderSubMenuLainnya extends React.Component{
  newsFeedsListTitle(){
    if(this.props.lastestNewsIndeks.fetched === true ){
      return(
        this.props.lastestNewsIndeks.indeks.map(lastestNewsIndek => {
          return(
            <div key={lastestNewsIndek._id} > 
              <Link to={`/${lastestNewsIndek.slug}`}>{lastestNewsIndek.title}</Link>
            </div>
          )
        })
      )
    }else{
      return(
        <p>......</p>
      )
    }
  }
  render(){
    const styles = {
      hotBadge : {
        backgroundColor: '#faad14',
        fontSize: '10px',
        marginLeft: '3px',
        marginTop: '-13px',
        padding: '3px 7px',
        zIndex: '1'
      }
    }
    return(
      <div >
        <Col span={24} className="submenu-lainnya-container">
          <div style={{height:'42px',background:'rgba(0, 0, 0, 0.45)'}}/>
          <div style={{background: 'rgba(0,0,0,.9)',height:'520px',marginTop:'-1px'}}>
            <Row gutter={16} className="list-submenu" style={{marginLeft: '40px',marginRight: '40px'}}>
              <Col span={4} className="title-menu">Nasional
              <ul className="list-menu">
                <li><a href="">Hukum</a></li>
                <li><a href="">Sosial</a></li>
                <li><a href="">Politik</a></li>
                <li><a href="">Sosial Budaya</a></li>
                <li><a href="">Metropolitan</a></li>
                <li><a href="">Kinerja Kabinet</a></li>
              </ul>
              </Col>
              <Col span={4} className="title-menu">Internasional
                <ul className="list-menu">
                  <li><a href="">Economic</a></li>
                  <li><a href="">Politics</a></li>
                </ul>
              </Col>
              <Col span={4} className="title-menu">Raya Daerah
                <ul className="list-menu">
                  <li><a href="">Jawa</a></li>
                  <li><a href="">Kalimantan</a></li>
                  <li><a href="">Sumatra</a></li>
                  <li><a href="">Sulawesi</a></li>
                  <li><a href="">Irian Jaya</a></li>
                </ul>
              </Col>
              <Col span={4} className="title-menu">Raya Pop
                <ul className="list-menu">
                  <li><a href="">Entertainment</a></li>
                  <li><a href="">Event</a></li>
                  <li><a href="">Film</a></li>
                  <li><a href="">Gossip</a></li>
                  <li><a href="">Musik</a></li>
                  <li><a href="">Seleb</a></li>
                </ul>
              </Col>
              <Col span={4} className="title-menu">Raya Trip
                <ul className="list-menu">
                  <li><a href="">Culture</a></li>
                  <li><a href="">Foods</a></li>
                  <li><a href="">Hotel</a></li>
                  <li><a href="">Pedalaman</a></li>
                  <li><a href="">Traveling</a></li>
                </ul>
              </Col>
              <div className="divider"/>
              <Col span={4} className="title-menu">
                <p style={{fontSize:'10px',marginBottom:'-35px',color:'rgb(81, 197, 27)'}}>
                  Berita - berita yang Mendalam
                </p><br/>
                <p style={{fontSize:'26px',fontWeight:'900',float:'left'}}>
                FOKUS
                </p><br/>
              </Col>
            </Row>
            <Row gutter={16} className="list-submenu" style={{marginLeft: '40px',marginRight: '40px'}}>
              <Col span={4} className="title-menu">Raya Sport
                <ul className="list-menu">
                  <li><a href="">Bola</a></li>
                  <li><a href="">Basket</a></li>
                  <li><a href="">Bulu Tangkis</a></li>
                  <li><a href="">Formula 1</a></li>
                  <li><a href="">Golf</a></li>
                  <li><a href="">Moto GP</a></li>
                </ul>
              </Col>
              <Col span={4} className="title-menu">Raya Oto
                <ul className="list-menu">
                  <li><a href="">Manufaktur</a></li>
                  <li><a href="">Mobil</a></li>
                  <li><a href="">Motor</a></li>
                  <li><a href="">Modifikasi</a></li>
                </ul>
              </Col>
              <Col span={4} className="title-menu">Raya Channel
                <ul className="list-menu">
                  <li><a href="">Gallery
                  <span className="badge" style={styles.hotBadge}>Hot</span>
                  </a></li>
                  <li><a href="">Infographic</a></li>
                  <li><a href="">Video</a></li>
                </ul>
              </Col>
            </Row>
            <Col span={24} className="footer-submenu">
              <div style={{marginLeft:'6.6%',marginRight:'1%',float:'left',fontSize:'11px',color:'#ffffffc4'}}>
                <a className="btn-submenu">Berlangganan</a>
              </div>
              <div style={{fontSize:'12px',color:'#ffffffc4'}}>
                <div style={{float:'left', marginLeft: '10px'}}>
                  <div style={{left:'-60px'}}>
                    <Badge dot >
                      <Icon type="notification" style={{color:'white'}}/>
                    </Badge>
                    <span style={{marginLeft:'8px',color:'white'}}>
                      <span style={{fontWeight:'bold'}}>NewsFeed : </span>
                      {console.log(this.props.lastestNewsIndeks,'>>>')} 
                      <TextLoop 
                        style={{marginLeft:'5px'}}
                        springConfig={{ stiffness: 180, damping: 8 }}
                        >
                        {this.newsFeedsListTitle()}
                      </TextLoop>
                    </span>
                  </div>
                </div>
              </div>
            </Col>
          </div>
        </Col>
      </div>
    )
  }
}

const maoStateToProps = (state,props) => {
  return {
    lastestNewsIndeks : state.lastesNews
  }
}

export default connect(
  maoStateToProps
)(NavbarHeaderSubMenuLainnya)

// export default NavbarHeaderSubMenuLainnya