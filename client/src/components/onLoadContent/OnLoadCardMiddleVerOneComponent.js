import React from 'react';
import ContentLoader from "react-content-loader";

class OnLoadCardMiddleVerOne extends React.Component{
  render(){
    return(
      <div className="card-v1-wraper">
        <ContentLoader
          height={300}
          width={540}
          speed={3}
          primaryColor="#f3f3f3"
          secondaryColor="#ecebeb"
        >
          <rect x="0" y="0" rx="0" ry="0" width="540" height="300" />
        </ContentLoader>
      </div>
    )
  }
}

export default OnLoadCardMiddleVerOne