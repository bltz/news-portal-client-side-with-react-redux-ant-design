import React from 'react';
import ContentLoader from "react-content-loader";

class OnLoadCardMiddleVerTwo extends React.Component{
  render(){
    const styles = {
      cardVerTwoWidth : {
        width : this.props.widthStyle
      }
    }
    return(
      <div style={styles.cardVerTwoWidth} className="card-v2-wraper-container">
      <ContentLoader
        height={170}
        width={102}
        speed={1.5}
        primaryColor="#dadbde"
        secondaryColor="#e4e6e8"
      >
        <rect x="9.51" y="96.05" rx="0" ry="0" width="27.051199999999998" height="9.790000000000001" /> 
        <rect x="42.88" y="96" rx="0" ry="0" width="51.37469999999999" height="8.9" /> 
        <rect x="80.21" y="112.92" rx="0" ry="0" width="11.641499999999999" height="11.3" /> 
        <rect x="9.5" y="113.76" rx="0" ry="0" width="66.78359999999999" height="8.9" /> 
        <rect x="9.5" y="131.52" rx="0" ry="0" width="46.2384" height="8.9" /> 
        <rect x="60.84" y="131.52" rx="0" ry="0" width="30.8295" height="8.9" /> 
        <rect x="11.19" y="148.05" rx="0" ry="0" width="79.16219999999998" height="7" /> 
        <rect x="0" y="0" rx="0" ry="0" width="102" height="70" />
      </ContentLoader>
      </div>
    )
  }
}

export default OnLoadCardMiddleVerTwo