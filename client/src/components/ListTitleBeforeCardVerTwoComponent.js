import React from 'react';
import ContentLoader from "react-content-loader";

class ListTitleBeforeCardVerTwo extends React.Component{
  contentOnLoad(index){
    const hardcodeIterasi = [0,1,2]
    return(
      hardcodeIterasi.map( i => {
        return(
          <div key={i}>
            <ContentLoader
              height={31}
              width={540}
              speed={2}
              primaryColor="#f3f3f3"
              secondaryColor="#ecebeb"
            >
              <rect x="0" y="0" rx="0" ry="0" width="530" height="20" />
            </ContentLoader>
          </div>
        )
      })
    )
  }
  contentLoaded(){
    const listLimit3 = [this.props.listTitles.indeks[1],this.props.listTitles.indeks[2],this.props.listTitles.indeks[3]]
    return (
      listLimit3.map((list,index) => {
        if(list !== undefined){
          return(
            <h3 key={list._id}>
              <a href="">{list.title}</a>
            </h3>
          )
        }else{
          return this.contentOnLoad(index)
        }
      })
    )
  }
  render(){
    return(
      <div className="card-v2-list-title-wraper">
        {this.props.listTitles.fetched === true && this.props.listTitles.indeks ? this.contentLoaded() : this.contentOnLoad()}
      </div>
    )
  }
}

export default ListTitleBeforeCardVerTwo