import React from 'react';
import { Tabs } from 'antd';
import ContentListVerOne from './ContentListVerOneComponent'

import { connect } from 'react-redux'

const TabPane = Tabs.TabPane;

class LeftSidebar extends React.Component{
  render(){
    return(
      <div className="leftsidebar-wraper">
        <Tabs 
          type="line"
          tabPosition="top" 
          size="small"
          tabBarStyle={{
            color : 'white',
          }}
          onNextClick={console.log('next')}
        >
          <TabPane tab={<span>POPULAR</span>} key="1">
            <ContentListVerOne DataIndeks={this.props.popularNewsIndex}/>
          </TabPane>
          <TabPane tab={<span>SOROT</span>} key="2">
            <ContentListVerOne DataIndeks={this.props.lastestNewsIndex}/>
          </TabPane>
          <TabPane tab={<span>TRENDING</span>} key="3">
            <ContentListVerOne DataIndeks={[]}/>
          </TabPane>
          <TabPane tab={<span>TOP COMMENT</span>} key="4">
            <ContentListVerOne DataIndeks={[]}/>
          </TabPane>
        </Tabs>
      </div>
    )
  }
}

const mapStateToProps = (state,props) => {
  return {
    lastestNewsIndex : state.lastesNews,
    popularNewsIndex : state.popularNews
  }
}

export default connect(
  mapStateToProps,
)(LeftSidebar);