import React from 'react';
import { Layout, Row, Col, Icon, Input, Divider } from 'antd';
import LoadingBar from 'react-redux-loading-bar';
import { Link } from 'react-router-dom';
import UserLoginModal from './modal/UserLoginModalComponent'
import UserLoginButton from './button/UserLoginButtonComponent'
import UserActiveButton from './button/UserActiveButtonComponent'
import PanelResultSearchBox from './PanelResultSearchBoxComponent'
import {connect} from 'react-redux'
import { partialSearchRequest } from '../redux/actions/search-action'

const { Header } = Layout;
const Search = Input.Search;

class Topheader extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      modalIsOpen: false,
      searchResultbox : false,
      // valueSearch: ''
    };
  }
  modalLogin(){
    return(
      <UserLoginModal 
        handleClosed={()=>{this.setState({modalIsOpen:false})}} 
        closeReq={()=>{this.setState({modalIsOpen:false})}}  
        status={this.state.modalIsOpen}
      />
    )
  }
  panelSearch(){
    return <PanelResultSearchBox listPartial={this.props.partialSearchResult.partial} valuePartial={this.props.partialSearchResult.valuePartialSearch}/>
  }
  withSearchButton(value){
    // console.log(value)
  }
  partialSearchIndicator(value){
    // this.props.handlePartialSearchRequest(value)
    // this.setState({valueSearch:value})
    // this.props.handlePartialSearchRequest(value)
    // const indicator = <Icon type="loading" className="spiner-indicator-search" key={1}/>
    // let value = this.state.valueSearch
    // if(this.state.valueSearch){
    //   if(this.state.valueSearch.length % 2 !== 0){
    //     return indicator
    //   }else if(this.state.valueSearch.length % 2 === 0){
    //     console.log(value,'<>>>>>>>>>')
    //     this.withSearchButton()
    //     // this.props.handlePartialSearchRequest(value)
    //   }
    // }
  }
  componentWillReceiveProps(nextProps){
    console.log(nextProps.partialSearchResult,'?x?x?x?')
    if(nextProps.partialSearchResult.partial){
      this.setState({searchResultbox:true})
    } 
    if(nextProps.partialSearchResult.panel === 'close'){
      console.log(nextProps.partialSearchResult.partial,'????')
      this.setState({searchResultbox:false})
    }
    
  }
  render(){
    return(
      <div>
        <Row>
          <Col span={24} style={{backgroundColor:'white'}}>
            <LoadingBar showFastActions style={{ width:'100%', backgroundColor: 'red', height: '2px' }} />
          </Col>
        </Row>
        <Header className="top-header-wraper">
          <Row>
            <Col style={{textAlign: 'right'}} span={18} push={6}>
              <Search
              // prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                // suffix={this.partialSearch()}
                placeholder="Search ..."
                onSearch={value => this.withSearchButton(value)}
                // onChange={value => this.setState({valueSearch:value.target.value})}
                onChange={value => this.props.handlePartialSearchRequest(value.target.value)}
                // onChange={value => this.partialSearch(value.target.value)}
                value={this.props.partialSearchResult.valuePartialSearch}
                style={{ width: 360, marginRight: '7px' }}
                size='default'
                enterButton
              />
              {this.partialSearchIndicator()}
              {this.state.searchResultbox === true ? this.panelSearch() : <div style={{display:'none'}}/>}
              <Divider type="vertical" className="divider-top-header"/>
              <div className="logo-sosmed-wraper">
                <a href="https://www.youtube.com/rayapos/" className="login100-social-item-header bg4" title="youtube" aria-label="youtube">
                  <Icon type="youtube" />
                </a>
                <a href="https://www.facebook.com/rayapos/" className="login100-social-item-header bg4" title="facebook" aria-label="facebook">
                  <Icon type="facebook" />
                </a>
                <a href="https://twitter.com/@rayapos" className="login100-social-item-header bg4" title="twitter" aria-label="twitter">
                  <Icon type="twitter" />
                </a>
                <a href="https://plus.google.com/+RayaPosonline" className="login100-social-item-header bg4" title="google+" aria-label="google+">
                  <Icon type="google" />
                </a>
                <a href="/" className="login100-social-item-header bg4" title="instagram" aria-label="instagram">
                  <Icon type="instagram" />
                </a>
              </div>
              <Divider type="vertical" className="divider-top-header"/>
              {localStorage.getItem('token') === null ? <UserLoginButton openModal={()=>{this.setState({modalIsOpen:true})}}/> : <UserActiveButton/>}
            </Col>
            <Col span={6} pull={18}>
              <Link to="/" className="logo" title={`rayapos.id`} aria-label={`rayapos.id`}>
                <img style={{height: '44px'}} alt="rayapos" src={this.props.logo}/>
              </Link>
            </Col>
          </Row>
          {this.modalLogin()}
        </Header>
      </div>
    )
  }
}

const mapStateToProps = (state,props) => {
  return {
    partialSearchResult : state.search
  }
}

const mapActionsToProps = {
  handlePartialSearchRequest : partialSearchRequest
}

export default connect(
  mapStateToProps,
  mapActionsToProps
)(Topheader)