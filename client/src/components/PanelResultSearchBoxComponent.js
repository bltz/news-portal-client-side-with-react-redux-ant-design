import React from 'react';
import { Col, Icon, Divider, Tooltip } from 'antd';
import { Link } from 'react-router-dom';
import moment from 'moment';
import 'moment/locale/id';
import noImage from '../assets/img/no-image.png'

class PanelResultSearchBox extends React.Component{
  
  thumbCondition(listResult){
    let imageSource = ''
    if(listResult.thumbnail != null){
      imageSource = `http://localhost:3000/${listResult.thumbnail.path}`
    }else{
      imageSource = noImage
    }
    return imageSource
  }
  lengthTitle(title){
    if(title.length >= 65){
      return title.slice(0, 65) + '...'
    }else{
      return title
    }
  }
  listItemPartialSearch(){
    return(
      this.props.listPartial.map( (listResult,i) => {
        return(
          <div key={listResult._id} className="search-list-item">
            <Link to={`/${listResult.slug}`}>
              <div className="search-list-item-wrap">
                {/* <div className="search-list-img">
                  <img style={{height: '60px', width: '90px'}} alt="thumb" src={this.thumbCondition(listResult)}/>
                </div> */}
                <div className="post-cover-search">
                  <div className="post-cover-parent-search">
                    <div className="post-cover-child-search" 
                      style={{backgroundImage:`url(${this.thumbCondition(listResult)})`}}
                    />		
                  </div> 
                </div> 
                <div className="search-list-main">
                  <div className="search-list-meta-content">
                    <div className="uppercoups">{listResult.uppercoupID ? listResult.uppercoupID.name : null}</div>
                    <div className="meta-title">
                      {this.lengthTitle(listResult.title)}
                    </div>
                    <div className="meta-category-date">
                      <span style={{fontWeight:'300'}}>{listResult.parentCategoryID.name.toUpperCase()}</span>
                      <Divider type="vertical" style={{margin: '0 5px'}}/>
                      <span >{moment(listResult.createdAt).locale('id').format('dddd, Do MMMM YYYY- LT') + ' WIB'}</span>
                    </div>
                  </div>
                </div>
              </div>
            </Link>
          </div>
        )
      })
    )
  }
  noResult(){
    return(
      <div style={{marginLeft:'15px'}}>
        <Icon type="frown-o" style={{fontSize:'20px'}}/> Result is not match
      </div>
    )
  }
  selengkapnya(){
    console.log('selengkapnya')
  }
  render(){
    const styles = {
      boxContainer : {
        position : 'absolute',
        zIndex : '13',
        top : '55px',
        left: '13%'
      },
      boxWraper : {
        marginTop:'-1px',
        width: '400px',
        textAlign : 'left'
      },
      btnSlengkapnya : {
        height: '32px',
        textAlign : 'right',
        verticalAlign: 'middle',
        lineHeight: '25px',
        margin: '5px 18px 0 0'
      },
      btnSlengkapnyaA : {
        fontWeight : '900',
        fontSize : '11px',
        color : '#595959',
        cursor : 'pointer'
      }
    }
    return(
      <div style={styles.boxContainer} className="box-search-container">
        <Col span={8} style={styles.boxWraper}>
          {this.props.listPartial.response ? this.noResult() : this.listItemPartialSearch()}
          {/* {this.listItemPartialSearch()} */}
          <div style={styles.btnSlengkapnya}>
          
            <a 
              href={`/search?q=${this.props.valuePartial}`}
              style={styles.btnSlengkapnyaA}
              // onCLick={this.selengkapnya()}
            >{console.log(this.props.valuePartial,':::::')}
                <Tooltip 
                placement="topRight" 
                title={'Cari Berita Lebih Lengkap'}
                mouseEnterDelay={0.2}
              >
                Selengkapnya <Icon type="right-circle-o"/>
              </Tooltip>
            </a>
            
        </div>
        </Col>
      </div>
    )
  }
}

export default PanelResultSearchBox