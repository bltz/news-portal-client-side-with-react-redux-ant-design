import React from 'react';
import moment from 'moment';
import 'moment/locale/id';
import ContentLoader from "react-content-loader";

class ContentListVerOne extends React.Component{
  contentOnLoad(){
    const onLoadIndex = [1,2,3,4,5]
    return(
      onLoadIndex.map(i => {
        return (
          <a href="" className="post-grid" key={i} style={{padding:'0', height:'112px'}}>
            <div style={{width:'290px', marginLeft:'7px', marginTop:'-5px'}}  >
              <ContentLoader
                height={112}
                width={287}
                speed={2}
                primaryColor="#8b8b8c"
                secondaryColor="#6f6f70"
              >
                <rect x="100.5" y="20" rx="1" ry="1" width="43.218" height="10" /> 
                <rect x="150.43" y="20" rx="1" ry="1" width="61.74" height="10" /> 
                <rect x="219.08" y="20" rx="1" ry="1" width="6.1739999999999995" height="10" /> 
                <rect x="109.86" y="40" rx="1" ry="1" width="80.262" height="10" /> 
                <rect x="197.24" y="40" rx="1" ry="1" width="80.262" height="10" /> 
                <rect x="109.86" y="60" rx="1" ry="1" width="55.566" height="10" /> 
                <rect x="172.27" y="60" rx="1" ry="1" width="37.044" height="10" /> 
                <rect x="215.96" y="60" rx="1" ry="1" width="37.044" height="10" /> 
                <rect x="100.5" y="80" rx="1" ry="1" width="18.522" height="10" /> 
                <rect x="2.5" y="15.05" rx="1" ry="1" width="90" height="90" />
              </ContentLoader>
            </div>
          </a>
        )
      })
    )
  }
  thumbCondition(dataIndek){
    let imgUrl = 'http://bourse-dechets-martinique.fr/wp-content/uploads/2014/10/no-image.png'
    let imageSource = ''
    if(dataIndek.thumbnail != null){
      imageSource = `http://localhost:3000/${dataIndek.thumbnail.path}`
    }else{
      imageSource = imgUrl
    }
    return imageSource
  }
  contentCondition(){
    this.props.DataIndeks.indeks.pop()
    return (
      this.props.DataIndeks.indeks.map( (dataIndek, i) => {
        return(
          <a href="" className="post-grid" key={dataIndek._id}>
            <div className="post-cover">
              <div className="post-cover-parent">
                <div className="post-cover-child" 
                  style={{backgroundImage:`url(${this.thumbCondition(dataIndek)})`}}
                >
                <p className="list-number" href="">
                  <span>{`0${i+1}`}</span>
                </p>
                </div> 		
              </div> 
            </div> 
            <span className="badge">{i+1}</span>
            <div className="post-desc">
              <span className="categories">{dataIndek.parentCategoryID.name.toUpperCase()}</span>
              <h6 className="uppercoups">{dataIndek.uppercoupID ? dataIndek.uppercoupID.name : null}</h6>
              <h2>{dataIndek.title.slice(0, 37) + '...'}</h2>
              <span className="date">{moment(dataIndek.createdAt).locale('id').format('dddd, Do MMMM YYYY- LT') + ' WIB'}</span>
            </div>  		
          </a>
        )
      })
    )
  }
  render(){
    return(
      <div className="list-content">
        {this.props.DataIndeks.fetched === true ? this.contentCondition() : this.contentOnLoad()}
      </div>
    )
  }
}

export default ContentListVerOne