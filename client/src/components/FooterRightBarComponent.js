import React from 'react'
import logoAppstore from '../assets/img/google_play.png'
import logoGoogleplay from '../assets/img/appstore.png'

class FooterRightBar extends React.Component{
  render(){
    return(
      <div className="panel-widget">
        <a 
          href="" 
          target="_blank" 
          rel="nofollow" 
          className="btn-img"
          title={`Download on App Store`} 
          aria-label={`Download on App Store`}
        >
          <img src={logoAppstore} alt="app-store"/>
        </a>
        <a 
          href="" 
          target="_blank" 
          rel="nofollow" 
          className="btn-img"
          title={`Download on Google Play`} 
          aria-label={`Download on Google Play`}
        >
          <img src={logoGoogleplay} alt="google-play"/>
        </a>
      </div>
    )
  }
}

export default FooterRightBar