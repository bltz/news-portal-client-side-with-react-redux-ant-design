import React from 'react';

class HeadIndexRightBar extends React.Component{
  render(){
    return(
      <small className="right-advertise">
        <a 
          className="text-muted" 
          href="https://rayapos.id"
          title={`Advertise with Us`} 
          aria-label={`Advertise with Us`} 
        >
          Advertise with Us
        </a>
      </small>
    )
  }
}

export default HeadIndexRightBar