import React from 'react'

class Nasional extends React.Component{
  render(){
    const styles = {
      hotBadge : {
        backgroundColor: '#faad14',
        height: '12px',
        lineHeight: '12px',
        fontSize: '9px',
        marginLeft: '3px',
        marginTop: '-13px',
        padding: '0 5px',
        zIndex: '1'
      }
    }
    return(
      <div className="submenu-nav">
        <ul>
          <li><a href="">Hukum</a></li>
          <li><a href="">Sosial</a></li>
          <li><a href="">Politik</a></li>
          <li><a href="">Sosial Budaya</a></li>
          <li><a href="">Metropolitan
          <span className="badge" style={styles.hotBadge}>Hot</span>
          </a></li>
          <li><a href="">Kinerja Kabinet</a></li>
        </ul>
      </div>
    )
  }
}

export default Nasional