import React from 'react'

class RayaDaerah extends React.Component{
  render(){
    return(
      <div className="submenu-nav">
        <ul>
          <li><a href="">Jawa</a></li>
          <li><a href="">Kalimantan</a></li>
          <li><a href="">Sumatra</a></li>
          <li><a href="">Sulawesi</a></li>
          <li><a href="">Irian Jaya</a></li>
        </ul>
      </div>
    )
  }
}

export default RayaDaerah