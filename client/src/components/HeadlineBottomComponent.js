import React from 'react';
import { Row, Col } from 'antd';
import HeadIndex from './HeadIndexComponent'
import CardInMiddleVerTwo from './CardInMiddleVerTwoComponent'
import OnLoadCardMiddleVerTwo from './onLoadContent/OnLoadCardMiddleVerTwoComponent'

import {connect} from 'react-redux'

class HeadlineBottom extends React.Component{
  render(){
    return(
      <div className="headLine-bottom-wraper-container">
        <div className="headLine-bottom-wraper">
          <Row>
            <Col span={24}>
              <div className="headLine-bottom-box">
                <h2>The standard Lorem Ipsum passage, used since the 1500s</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
              </div>
            </Col>
          </Row>
        </div>
        <div className="headLine-bottom-box-list-row">
          <Row>
            <Col span={24}>
              <div className="headLine-bottom-box-list">
                <HeadIndex title='raya sport'/>
                <HeadIndex title='raya oto'/>
                <HeadIndex title='raya trip'/>
                <HeadIndex title='raya channel'/>
              </div>
            </Col>
          </Row>
        </div>
        <div style={{height: '5px'}}/>
        <div className="headLine-bottom-box-list-row">
          <Row>
            <Col span={24}>
              <div className="headLine-bottom-box-list">
                {this.props.rayaSportNewsIndex.fetched === true ? <CardInMiddleVerTwo widthStyle='24%' dataIndek={this.props.rayaSportNewsIndex.indeks[0]}/> : <OnLoadCardMiddleVerTwo widthStyle='24%'/>}
                {this.props.rayaOtoNewsIndex.fetched === true ? <CardInMiddleVerTwo widthStyle='24%' dataIndek={this.props.rayaOtoNewsIndex.indeks[0]}/> : <OnLoadCardMiddleVerTwo widthStyle='24%'/>}
                {this.props.rayaTripNewsIndex.fetched === true ? <CardInMiddleVerTwo widthStyle='24%' dataIndek={this.props.rayaTripNewsIndex.indeks[0]}/> : <OnLoadCardMiddleVerTwo widthStyle='24%'/>}
                {this.props.rayaChannelNewsIndex.fetched === true ? <CardInMiddleVerTwo widthStyle='24%' dataIndek={this.props.rayaChannelNewsIndex.indeks[0]}/> : <OnLoadCardMiddleVerTwo widthStyle='24%'/>}
              </div>
            </Col>
          </Row>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state,props) => {
  return{
    rayaSportNewsIndex : state.rayaSportNews,
    rayaOtoNewsIndex : state.rayaOtoNews,
    rayaTripNewsIndex : state.rayaTripNews,
    rayaChannelNewsIndex : state.rayaChannelNews
  }
}

export default connect(
  mapStateToProps
)(HeadlineBottom)