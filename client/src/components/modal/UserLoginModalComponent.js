import React from 'react';
import { Icon, Divider, Checkbox, message } from 'antd';
import ReactModal from 'react-modal';
import backgroundLogin from '../../assets/bg-01.jpg'
import LoadingBar from 'react-redux-loading-bar';
import FacebookLogin from 'react-facebook-login';
import { GoogleLogin } from 'react-google-login';
import { connect } from 'react-redux'
import { updateUsername, updatePassword, loginRequest, resetStatusLogin, loginRequestWithSosmed } from '../../redux/actions/userLogin-action'

ReactModal.setAppElement('body')


class UserLoginModal extends React.Component{
  constructor(props){
    super(props)
    this.handleUpdateUserName = this.handleUpdateUserName.bind(this)
    this.handleUpdatePassword = this.handleUpdatePassword.bind(this)
    this.handleLoginRequest   = this.handleLoginRequest.bind(this)
    this.handleResetStatusLogin = this.handleResetStatusLogin.bind(this)
  }
  handleResetStatusLogin = () => {
    this.props.handleResetStatusLogin()
  }
  handleUpdateUserName = (event) => {
    this.props.handleUpdateUserName(event.target.value)
  }
  handleUpdatePassword = (event) => {
    this.props.handleUpdatePassword(event.target.value)
  }
  handleLoginRequest = (event) => {
    event.preventDefault()
    this.props.handleLoginRequest(this.props.userLoginData.username, this.props.userLoginData.password)
  }
  avatarCondition(nextProps){
    let avatarUrl = ''
    if(nextProps.userLoginData.loginWith === 'facebook' || nextProps.userLoginData.loginWith === 'google'){
      avatarUrl = `${nextProps.userLoginData.avatar}`
    }else{
      avatarUrl = `http://localhost:3000/${nextProps.userLoginData.avatar}`
    }
    return avatarUrl
  }
  componentWillReceiveProps(nextProps){
    console.log(nextProps.userLoginData,'nextprops')
    const warning = (response) => {
      message.warning(response, 5);
      this.handleResetStatusLogin()
    };
    
    const success = (response) => {
      const hide = message.loading('Login in progress..', 0);
      // Dismiss manually and asynchronously
      setTimeout(hide, 2500);
    };
    if(nextProps.userLoginData.responseStatus === 202){
      warning(nextProps.userLoginData.responseMessage)
    }else if(nextProps.userLoginData.responseStatus === 200){
      success(nextProps.userLoginData.responseMessage)
      if(nextProps.userLoginData.succesLogin === true){
        localStorage.setItem('token', nextProps.userLoginData.token)
        localStorage.setItem('fullname', nextProps.userLoginData.fullname)
        localStorage.setItem('avatar', this.avatarCondition(nextProps))
        setTimeout(window.location.assign("/"), 1000);
      }
    }
  }
  LoginWithSosmed(res, type) {
    let payload;
    if (type === 'facebook' && res.email) {
      payload = {
          name: res.name,
          provider: type, // to db => via
          email: res.email,
          provider_id: res.id, // to viaId
          token: res.accessToken, // token session exp 4534
          provider_avatar: res.picture.data.url // avatar
      };
    }

   if (type === 'google' && res.w3.U3) {
   payload = {
     name: res.w3.ig,
     provider: type,
     email: res.w3.U3,
     provider_id: res.El,
     token: res.Zi.access_token,
     provider_avatar: res.w3.Paa
   };
  }

  // console.log(`${type} console`);
  // console.log(payload);

  if(payload) {
    this.props.handleLoginRequestWithSosmed(payload)
  } 
}
  render(){
    const responseFacebook = (response) => {
      console.log('console facebook');
      console.log(response);
      // this.signup(response, 'facebook');
    }
    //callback response google
    const responseGoogle = (response) => {
      // console.log('console google +');
      // console.log(response);
      this.LoginWithSosmed(response, 'google');
    }
    return(
      <div>
        <ReactModal
          isOpen={this.props.status}
          onRequestClose={this.props.closeReq}
          contentLabel="Example Modal"
          className="modal-login"
          overlayClassName="modal-login-overlay"
        >
          <div className="wrap-login-form">
          {/* {console.log(this.props.userLoginData,'>>>')} */}
            <div className="login-form-title" style={{backgroundImage:`url(${backgroundLogin})`}}>
            <div className="close-box-in" onClick={this.props.handleClosed}>
              <Icon type="close" style={{
                color: 'white',
                fontSize: '22px',
                fontWeight: '300',
              }}/>
            </div>
              <span className="login-form-title-txt font-global-form-login">
                Login
              </span>
              
            </div>
            <LoadingBar showFastActions style={{ width:'100%', backgroundColor: 'red', height: '2px' }} />
            <form className="login-form" name="form" onSubmit={this.handleLoginRequest}>
              <div className="username-label">
                <span className="style-label font-global-form-login">
                  Username
                </span>
              </div>
              <div className="wrap-input">
                <input className="wrap-input" 
                  type="text" 
                  name="username" 
                  onChange={this.handleUpdateUserName} 
                  placeholder={this.props.userLoginData.username}
                  // value={this.props.userLoginData.username}
                required/>
              </div>
              <div className="username-label">
                <span className="style-label font-global-form-login">
                   Password
                </span>
              </div>
              <div className="wrap-input">
                <input className="wrap-input" 
                  type="password" 
                  name="password" 
                  onChange={this.handleUpdatePassword}
                  placeholder={this.props.userLoginData.password}
                  // value={this.props.userLoginData.password} 
                required/>
              </div>
              <div style={{textAlign: 'right',paddingBottom: '10px', paddingTop: '10px'}}>
                <Checkbox style={{fontSize: '12px',color: '#555555',float:'left'}} checked>Ingat Password</Checkbox>
                <a 
                  href="http://localhost:3000/user/reset" 
                  style={{fontSize: '12px',color: '#555555'}} 
                  className="font-global-form-login"
                  title="Lupa Password?" aria-label="Lupa Password?"
                >
                  Lupa Password?
                </a>
              </div>
              <div className="wrap-form-btn" style={{marginBottom:'20px'}}>
                <button type="submit" value="Submit" className="login-form-btn font-global-form-login">
                  Sign In
                </button>
              </div>
              <Divider style={{fontSize: '11px', padding: '10px 0', color:'#555555'}}>
                atau login menggunakan
              </Divider>
              {/* <a 
                href=""  
                style={{color: '#fff', backgroundColor: '#3b5998', marginBottom:'10px'}} 
                className="login-form-btn font-global-form-login"
                title="Login Facebook" aria-label="Login Facebook"
              >
                <Icon type="facebook" style={{fontSize: '16px',marginRight: '8px'}}/>
                Facebook
              </a>
              <a 
                href="" 
                style={{color: '#fff', backgroundColor: '#f73032', marginBottom:'10px'}} 
                className="login-form-btn font-global-form-login"
                title="Login Google" aria-label="Login Google"
              >
                <Icon type="google" style={{fontSize: '16px',marginRight: '8px'}}/>
                Google
              </a> */}
              <FacebookLogin
                appId="338682129874902"
                autoLoad={false}
                icon={<Icon type="facebook" style={{fontSize: '20px', margin: '0 10px 0 0'}}/>}
                textButton='Facebook'
                cssClass='login-form-btn-fb font-global-form-login'
                fields="name,email,picture.width(300).height(300),verified,gender"
                callback={responseFacebook}
              />
              <GoogleLogin
                style={{marginBottom:'10px'}} 
                clientId={'86490534860-gjledhpj6oloktbbkbepedhismafb0g1.apps.googleusercontent.com'}
                className='login-form-btn-google font-global-form-login'
                autoLoad={false}
                buttonText=''
                onSuccess={responseGoogle}
                onFailure={responseGoogle}
              >
              <Icon type="google" style={{fontSize: '20px', margin: '0 10px 0 0'}}/> Google
              </GoogleLogin>
              <div className="login-footer">
                <span className="font-global-form-login" style={{color: '#555555',lineHeight: '1.5',fontSize:'11px',}}>
                  rayapos © 2018
                </span>
              </div>
            </form>
          </div>
        </ReactModal>
      </div>
    )
  }
}

const mapStateToProps = (state,props) => {
  return{
    userLoginData : state.userLogin
  }
}

const mapActionToProps = {
  handleUpdateUserName : updateUsername,
  handleUpdatePassword : updatePassword,
  handleLoginRequest : loginRequest,
  handleResetStatusLogin : resetStatusLogin,
  handleLoginRequestWithSosmed : loginRequestWithSosmed

}

export default connect(
  mapStateToProps,
  mapActionToProps
)(UserLoginModal)