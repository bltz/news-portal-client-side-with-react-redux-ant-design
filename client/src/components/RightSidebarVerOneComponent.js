import React from 'react';
import CardInRightVerOne from './CardInRightVerOneComponent'
import InlineRectangleAdv from './advBanner/InlineRectangleAdvComponent'
import FooterRightBar from './FooterRightBarComponent'
import HeadIndexRightBar from './HeadIndexRightBarComponent'
import { connect } from 'react-redux'

class RightSidebarVerOne extends React.Component{
  render(){
    return(
      <div className="right-sidebar-v1-wraper">
        <HeadIndexRightBar/>
        <div className="panel-title-right-bar"><strong>Advertisement</strong></div>
        <InlineRectangleAdv/>
        <CardInRightVerOne dataIndeks={this.props.sponsoredArticle}/>
        <FooterRightBar/>
      </div>
    )
  }
}

const mapStateToProps = (state,props) => {
  return {
    sponsoredArticle : state.sponsoredNews
  }
}

export default connect(
  mapStateToProps
)(RightSidebarVerOne)