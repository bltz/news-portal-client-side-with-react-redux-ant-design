import React from 'react';
import { Row, Col, Divider } from 'antd';
import { Link } from 'react-router-dom';

class Footer extends React.Component{
  render(){
    return(
      <div className="footer">
        <div className="footer-top">
          <Row>
            <Col style={{textAlign: 'right'}} span={18} push={6}>
              <div>
              </div>
            </Col>
            <Col span={6} pull={18}>
              <Link 
                className='logo' 
                to="/"
                title={`rayapos.id`} 
                aria-label={`rayapos.id`}
              >
                <img style={{height: '62px'}} alt="rayapos" src={this.props.logo}/>
              </Link>
            </Col>
          </Row>
        </div>
        <div className="footer-bottom">
          <Row>
            <Col className="footer-navbar" span={18}>
              <a href="" title="Indeks Berita" aria-label="Indeks Berita">Indeks</a>
              <Divider type="vertical" className="divider-footer"/>
              <a href="" title="Tentang rayapos.id" aria-label="Tentang rayapos.id">Tentang Kami</a>
              <Divider type="vertical" className="divider-footer"/>
              <a href="" title="Redaksi rayapos.id" aria-label="Redaksi rayapos.id">Redaksi</a>
              <Divider type="vertical" className="divider-footer"/>
              <a href="" title="Kontak" aria-label="Kontak">Kontak</a>
              <Divider type="vertical" className="divider-footer"/>
             <a href="" title="Pedoman Media Siber" aria-label="Pedoman Media Siber">Pedoman Media Siber</a>
              <Divider type="vertical" className="divider-footer"/>
              <a href="" title="Privacy Policy" aria-label="Privacy Policy">Privacy Policy</a>
              <Divider type="vertical" className="divider-footer"/>
              <a href="" title="Disclaimer" aria-label="Disclaimer">Disclaimer</a>
              <Divider type="vertical" className="divider-footer"/>
              <a href="" title="Karir" aria-label="Karir">Karir</a>
              <Divider type="vertical" className="divider-footer"/>
              <a href="" title="Advertise" aria-label="Advertise">Advertise</a>
              <Divider type="vertical" className="divider-footer"/>
              <a href="" title="Sitemap" aria-label="Sitemap">Sitemap</a>
            </Col>
            <Col className="copyright">
              <a href="" title="Copyright" aria-label="Copyright">Copyright @2018 PT.Sinar Kokoh	Media</a>
            </Col>
          </Row>
        </div>
      </div>
    )
  }
}

export default Footer