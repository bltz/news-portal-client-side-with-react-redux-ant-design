import React from 'react';
import moment from 'moment';
import 'moment/locale/id';
import ContentLoader from "react-content-loader";

class CardInMiddleVerTwo extends React.Component{
  thumbCondition(dataIndek){
    let imgUrl = 'http://bourse-dechets-martinique.fr/wp-content/uploads/2014/10/no-image.png'
    let imageSource = ''
    if(dataIndek.thumbnail != null){
      imageSource = `http://localhost:3000/${dataIndek.thumbnail.path}`
    }else{
      imageSource = imgUrl
    }
    return imageSource
  }
  contentCondition(){
    return(
      <div>
        <div className="cardv2-img-wraper">
          <div className="cardv2-img-parent">
            <div className="cardv2-img-child"
              style={{backgroundImage:`url(${this.thumbCondition(this.props.dataIndek)})`}}
            />
          </div>
        </div>
        <div className="card-v2-desc-wraper">
          <span className="card-v2-desc-categories">{this.props.dataIndek.parentCategoryID.name.toUpperCase()}</span>
          <h3 className="card-v2-desc-title">{this.props.dataIndek.title.slice(0,20)+' ...'}</h3>
          <span className="card-v2-desc-date">{moment(this.props.dataIndek.createdAt).locale('id').format('dddd, Do MMMM YYYY- LT') + ' WIB'}</span>
        </div>
      </div>
    )
  }
  contentOnLoad(){
    return(
      <ContentLoader
        height={170}
        width={102}
        speed={1.5}
        primaryColor="#dadbde"
        secondaryColor="#e4e6e8"
      >
        <rect x="9.51" y="96.05" rx="0" ry="0" width="27.051199999999998" height="9.790000000000001" /> 
        <rect x="42.88" y="96" rx="0" ry="0" width="51.37469999999999" height="8.9" /> 
        <rect x="80.21" y="112.92" rx="0" ry="0" width="11.641499999999999" height="11.3" /> 
        <rect x="9.5" y="113.76" rx="0" ry="0" width="66.78359999999999" height="8.9" /> 
        <rect x="9.5" y="131.52" rx="0" ry="0" width="46.2384" height="8.9" /> 
        <rect x="60.84" y="131.52" rx="0" ry="0" width="30.8295" height="8.9" /> 
        <rect x="11.19" y="148.05" rx="0" ry="0" width="79.16219999999998" height="7" /> 
        <rect x="0" y="0" rx="0" ry="0" width="102" height="70" />
      </ContentLoader>
    )
  }
  render(){
    const styles = {
      cardVerTwoWidth : {
        width : this.props.widthStyle
      }
    }
    return(
      <div style={styles.cardVerTwoWidth} className="card-v2-wraper-container">
        {this.props.dataIndek ? this.contentCondition() : <div style={{display:'none'}}/>}
      </div>
    )
  }
}

export default CardInMiddleVerTwo