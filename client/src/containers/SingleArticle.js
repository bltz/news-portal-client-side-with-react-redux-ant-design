import React from 'react';
import { BackTop } from 'antd';
import logoHeader from '../assets/img/logonew.png';
import StickyNode from 'react-stickynode';
import TopHeader from '../components/TopHeaderComponent';
import NavbarHeader from '../components/NavbarHeaderComponent';
import ContentArticleDetail from '../components/ContentArticleDetailComponent'
import Footer from '../components/FooterComponent';
import { connect } from 'react-redux'
import { singleArticleRequest } from '../redux/actions/singleArticle-action'
import { cloasePanelSearch, resetValueSearch } from '../redux/actions/search-action'

let fetchOk = ''
let fetchMsg = ''
let singleArticleData = {}
let rejectFetchSingleArticleResponse = ''
let rejectFetchSingleArticleFindSlug = ''

class SingleArticle extends React.Component{
  componentWillMount(){
    this.props.handleResetValuePartialSearch()
  }
  contentCondition(){
    if(rejectFetchSingleArticleResponse === '::: NO DATA SINGLE NEWS BY SLUG :::'){
      window.location.assign("/");
      return <div>eroir</div>
    }else{
      return <ContentArticleDetail message={fetchMsg} response={singleArticleData}/>
      // fetchOk === 200  ? <ContentArticleDetail response={singleArticleData}/> : onloadSingleArticle
    }
  }
  componentDidUpdate(){
    // console.log(singleArticleData,'::::::::>><<><',rejectFetchSingleArticleResponse, '::::', rejectFetchSingleArticleFindSlug)
    // console.log(this.props.match.params.slug,'::::')
    if(singleArticleData === undefined && rejectFetchSingleArticleResponse === "::: NO DATA SINGLE NEWS BY SLUG :::"){
      // console.log('masuk',this.props.match.params.slug)
      if(this.props.match.params.slug !== rejectFetchSingleArticleFindSlug){
        this.props.handleSingleArticleRequest(this.props.match.params.slug)
      }
    }else{
      if(this.props.match.params.slug !== singleArticleData.slug){
        console.log(this.props.panelPartialSearch,'panel>>>')
        this.props.handleSingleArticleRequest(this.props.match.params.slug)
        if(this.props.panelPartialSearch === 'open'){
          this.props.handleCloasePanelSearch()
        }
      }
    }
  }
  componentDidMount(){
    this.props.handleSingleArticleRequest(this.props.match.params.slug)
  }
  componentWillReceiveProps(nextProps){
    // this.props.handleResetValuePartialSearch()
    fetchOk = nextProps.fetchSingleArticle.reqSingleArticle.status
    fetchMsg = nextProps.fetchSingleArticle.reqSingleArticle.msg
    singleArticleData = nextProps.fetchSingleArticle.reqSingleArticle.response
    rejectFetchSingleArticleResponse = nextProps.fetchSingleArticle.rejectSingleArticle.response.response
    rejectFetchSingleArticleFindSlug = nextProps.fetchSingleArticle.rejectSingleArticle.findBySLug
  }
  render(){
    return(
      <div className="RayaBody">
        <BackTop>
        {/* {console.log(fetchOk,'????')} */}
          <div className="ant-back-top-inner">UP</div>
        </BackTop>
        <StickyNode enabled={true} innerZ={3}>
          <TopHeader logo={logoHeader}/>
          <NavbarHeader/>
        </StickyNode>
        {/* {console.log(singleArticleData,'>>>????')} */}
        {/* <ContentArticleDetail /> */}
        {/* {this.contentCondition(fetchOk, singleArticleData)} */}
        {fetchOk === 200 ? <ContentArticleDetail message={fetchMsg} response={singleArticleData}/> :this.contentCondition()}
        <Footer logo={logoHeader}/>
      </div>
    )
  }
}

const mapStateToProps = (state,props) => {
  return {
    fetchSingleArticle : state.singleArticle,
    panelPartialSearch : state.search.panel
  }
}

const mapActionToProps = {
  handleSingleArticleRequest : singleArticleRequest,
  handleCloasePanelSearch : cloasePanelSearch,
  handleResetValuePartialSearch : resetValueSearch
}

export default connect(
  mapStateToProps,
  mapActionToProps
)(SingleArticle)