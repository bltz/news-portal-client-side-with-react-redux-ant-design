import React, { Component } from 'react';
import { BackTop } from 'antd';
import logoHeader from '../assets/img/logonew.png'
import StickyNode from 'react-stickynode'
import TopHeader from '../components/TopHeaderComponent'
import NavbarHeader from '../components/NavbarHeaderComponent'
import HeadlineTop from '../components/HeadlineTopComponent'
import Content from '../components/ContentComponent'
import HeadlineBottom from '../components/HeadlineBottomComponent'
import Footer from '../components/FooterComponent'
import BillboardAdv from '../components/advBanner/BillboardAdvComponent'


import { connect } from 'react-redux'
import { fetchHomeReq } from '../redux/actions/home-action'

class Home extends Component {
  constructor(props){
    super(props)
    this.state = {
      adsBillboard : true
    }
  }
  componentDidMount(){
    this.props.onApiRequest();
  }
  render() {
    const heightBillboardAdv = this.state.adsBillboard === true ? 250 : 0
    return (
      <div className="RayaBody">
        <BackTop>
          <div className="ant-back-top-inner">UP</div>
        </BackTop>
        <StickyNode enabled={true} innerZ={3}>
          <TopHeader logo={logoHeader}/>
          <NavbarHeader/>
        </StickyNode>
        {this.state.adsBillboard === true ? <BillboardAdv/> : <div style={{display:'none'}}/>}
        <HeadlineTop/>
        <Content plusHeight={heightBillboardAdv}/>
        <HeadlineBottom/>
        <Footer logo={logoHeader}/>
      </div>
    );
  }
}

const mapStateToProps = (state,props) => {
  return {
    lastestNewsIndex : state.lastesNews,
    popularNewsIndex : state.popularNews
  }
}

const mapActionsToProps = {
  onApiRequest : fetchHomeReq
}

export default connect(
  mapStateToProps,
  mapActionsToProps,
)(Home);
